package request

type Lang string

type LotTransl struct {
	Name        string
	Description string
}

func NewLotTransl(name string, description string) *LotTransl {
	return &LotTransl{Name: name, Description: description}
}

type Draw struct {
	WinningPlaces uint
}

func NewDraw(winningPlaces uint) *Draw {
	return &Draw{WinningPlaces: winningPlaces}
}

type Image struct {
	Id   *string
	Path *string
}

func NewImage(id *string, path *string) *Image {
	return &Image{Id: id, Path: path}
}
