package middlewares

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"shop-service/internal/shared/infra/ctxkey"
	"strings"
)

type CmsUserID string

func Auth(next func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenStr := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1)
		token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(os.Getenv("JWT_SECRET")), nil
		})
		if err != nil {
			unauthorizedResponse(w)
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			ctx := context.WithValue(r.Context(), ctxkey.ContextKeyCmsUserID, uint(claims["sub"].(float64)))
			next(w, r.WithContext(ctx))
		} else {
			unauthorizedResponse(w)
			return
		}
	}
}

func unauthorizedResponse(w http.ResponseWriter) {
	b, _ := json.Marshal(map[string]string{
		"error": "unauthorized",
	})
	w.WriteHeader(401)
	_, _ = w.Write(b)
}
