create table if not exists coins
(
    id                       uuid primary key,
    user_id                  integer      not null,
    amount                   integer      not null,
    accrued_at               timestamptz  not null,
    reason_id                varchar(100),
    reason_type              varchar(100) not null,
    reason_text_translations json         not null,
    app_slug                 varchar(50)  not null,
    created_at               timestamp,
    updated_at               timestamp
);
