package logger

import (
	"github.com/google/logger"
	"os"
)

var l *logger.Logger

func Instance() *logger.Logger {
	if l == nil {
		_ = os.Mkdir("./logs", 0755)
		lf, err := os.OpenFile("./logs/grpc_server.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		if err != nil {
			logger.Fatalf("Failed to open log file: %v", err)
		}
		l = logger.Init("GrpcLogger", false, false, lf)
	}
	return l
}
