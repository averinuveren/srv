package rpc

import (
	"encoding/json"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	coinApp "shop-service/internal/coin/app"
	coreApp "shop-service/internal/core/app"
	dtoResponse "shop-service/internal/core/app/dto/response"
	drawApp "shop-service/internal/draw/app"
	"shop-service/internal/shared/ui/rpc/input"
	pb "shop-service/internal/shared/ui/rpc/protocol"
)

type Service struct {
	validate *validator.Validate
	coreApp  coreApp.Application
	coinApp  coinApp.Application
	drawApp  drawApp.Application
}

func ProvideService(
	v *validator.Validate,
	coreApp coreApp.Application,
	coinApp coinApp.Application,
	drawApp drawApp.Application,
) pb.ShopServiceServer {
	return &Service{
		validate: v,
		coreApp:  coreApp,
		coinApp:  coinApp,
		drawApp:  drawApp,
	}
}

func (s *Service) Accrual(ctx context.Context, r *pb.AccrualRequest) (*pb.AccrualResponse, error) {
	var i input.AccrualInput
	if err := s.validateRequest(&i, r); err != nil {
		return nil, err
	}

	dto, err := i.ToAccrualDTO()
	if err != nil {
		errString := fmt.Sprintf("input to accrual dto failed: %s", err.Error())
		return nil, status.Error(codes.Internal, errString)
	}
	if err := s.coinApp.Accrual(ctx, *dto); err != nil {
		return nil, fmt.Errorf("accrual failed: %s", err)
	}
	return &pb.AccrualResponse{}, nil
}

func (s *Service) BuyLot(ctx context.Context, r *pb.BuyLotRequest) (*pb.Empty, error) {
	var i input.BuyLot
	if err := s.validateRequest(&i, r); err != nil {
		return nil, err
	}
	dto := i.ToBuyLotDTO()

	err := s.coreApp.BuyLot(ctx, *dto)
	switch true {
	case errors.Is(err, coreApp.LotNotFound):
		return nil, status.Error(codes.NotFound, "lot_not_found")
	case errors.Is(err, coreApp.LotInactive):
		return nil, status.Error(codes.FailedPrecondition, "lot_inactive")
	case errors.Is(err, coreApp.InsufficientCount):
		return nil, status.Error(codes.FailedPrecondition, "insufficient_count")
	case errors.Is(err, coreApp.PriceNotSet):
		return nil, status.Error(codes.FailedPrecondition, "price_not_set")
	case errors.Is(err, coreApp.NotEnoughCoinsForBuy):
		return nil, status.Error(codes.FailedPrecondition, "not_enough_coins")
	case errors.Is(err, coreApp.LotLimitPerUserExceeded):
		return nil, status.Error(codes.FailedPrecondition, "lot_limit_exceeded")
	case err != nil:
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.Empty{}, nil
}

func (s *Service) LotsForUserAllByPeriod(ctx context.Context, req *pb.LotsForUserAllByPeriodRequest) (*pb.LotsForUserAllByPeriodResponse, error) {
	lbp, err := s.coreApp.LotsForUserAllByPeriod(ctx, uint(req.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := new(pb.LotsForUserAllByPeriodResponse)

	// upcoming
	var upcoming []*pb.LotForUser
	for _, l := range lbp.Upcoming {
		upcoming = append(upcoming, lotForUserDTOToResponse(*l))
	}
	resp.Upcoming = upcoming

	// current
	var current []*pb.LotForUser
	for _, l := range lbp.Current {
		current = append(current, lotForUserDTOToResponse(*l))
	}
	resp.Current = current

	return resp, nil
}

func (s *Service) UserCoinsCount(ctx context.Context, r *pb.UserCoinsCountRequest) (*pb.UserCoinsCountResponse, error) {
	coinsCount, err := s.coinApp.GetUserCoinAmount(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.UserCoinsCountResponse{CoinsCount: int32(*coinsCount)}, nil
}

func (s *Service) UserTicketsCount(ctx context.Context, r *pb.UserTicketsCountRequest) (*pb.UserTicketsCountResponse, error) {
	tickets, err := s.drawApp.GetUserTickets(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.UserTicketsCountResponse{TicketsCount: uint32(len(tickets))}, nil
}

func (s *Service) GetActiveLotsWhereUserBuyTicket(ctx context.Context, r *pb.GetActiveLotsWhereUserBuyTicketRequest) (*pb.GetActiveLotsWhereUserBuyTicketResponse, error) {
	lots, err := s.coreApp.ActiveWhereUserBuyTicket(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var resp pb.GetActiveLotsWhereUserBuyTicketResponse
	for _, l := range lots {
		resp.Lots = append(resp.Lots, lotForUserDTOToResponse(*l))
	}
	return &resp, nil
}

func (s *Service) GetUserCoinsEarnings(ctx context.Context, r *pb.GetUserCoinsEarningsRequest) (*pb.GetUserCoinsEarningsResponse, error) {
	coinsEarnings, err := s.coinApp.UserEarnings(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.GetUserCoinsEarningsResponse{CoinsEarnings: int32(*coinsEarnings)}, nil
}

func (s *Service) GetUserCoinsSpent(ctx context.Context, r *pb.GetUserCoinsSpentRequest) (*pb.GetUserCoinsSpentResponse, error) {
	coinsSpent, err := s.coinApp.UserSpent(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.GetUserCoinsSpentResponse{CoinsSpent: int32(*coinsSpent)}, nil
}

func (s *Service) GetUserCoinHistory(ctx context.Context, r *pb.GetUserCoinHistoryRequest) (*pb.GetUserCoinHistoryResponse, error) {
	var resp pb.GetUserCoinHistoryResponse
	coins, err := s.coinApp.UserHistory(ctx, uint(r.UserId))
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	for _, dto := range coins {
		resp.Coins = append(resp.Coins, &pb.Coin{
			Id:         dto.ID,
			ReasonText: dto.ReasonText,
			AccruedAt:  dto.AccruedAt,
			Amount:     dto.Amount,
		})
	}
	return &resp, nil
}

func (s *Service) IsAccrualOnDate(ctx context.Context, r *pb.IsAccrualOnDateRequest) (*pb.IsAccrualOnDateResponse, error) {
	var i input.IsAccrualOnDateInput
	if err := s.validateRequest(&i, r); err != nil {
		return nil, err
	}

	res, err := s.coinApp.IsAccrualOnDate(ctx, *i.ToDTO())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &pb.IsAccrualOnDateResponse{Result: *res}, nil
}

func (s *Service) validateRequest(i interface{}, r interface{}) error {
	b, err := json.Marshal(r)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(b, &i); err != nil {
		return err
	}
	if err := s.validate.Struct(i); err != nil {
		return status.Error(codes.InvalidArgument, err.Error())
	}
	return nil
}

func lotForUserDTOToResponse(dto dtoResponse.LotForUserDTO) *pb.LotForUser {
	var price uint32
	if dto.Price != nil {
		price = uint32(*dto.Price)
	}

	var activeAt string
	if dto.ActiveAt != nil {
		activeAt = *dto.ActiveAt
	}

	var inactiveAt string
	if dto.InactiveAt != nil {
		inactiveAt = *dto.InactiveAt
	}

	var limitPerUser uint32
	if dto.LimitPerUser != nil {
		limitPerUser = uint32(*dto.LimitPerUser)
	}

	var draw *pb.Draw

	if dto.Draw != nil {
		draw = &pb.Draw{
			PrizesCount:        uint32(dto.Draw.PrizesCount),
			BoughtTicketsCount: uint32(dto.Draw.BoughtTicketsCount),
		}
	}

	return &pb.LotForUser{
		Id:            dto.ID,
		Name:          dto.Name,
		Description:   dto.Description,
		Price:         price,
		Count:         uint32(dto.Count),
		LeftoverCount: uint32(dto.LeftoverCount),
		ActiveAt:      activeAt,
		InactiveAt:    inactiveAt,
		LimitPerUser:  limitPerUser,
		Images:        dto.Images,
		ProductType:   dto.ProductType,
		Draw:          draw,
	}
}
