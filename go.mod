module shop-service

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/golang/protobuf v1.4.3
	github.com/google/logger v1.1.0
	github.com/google/uuid v1.1.2
	github.com/google/wire v0.4.0
	github.com/gorilla/mux v1.7.4
	github.com/imdario/mergo v0.3.11
	github.com/jackc/pgtype v1.6.2
	github.com/jackc/pgx/v4 v4.10.0
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
	google.golang.org/grpc v1.33.2
)
