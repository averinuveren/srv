package request

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"shop-service/internal/coin/domain"
	sharedDomain "shop-service/internal/shared/domain"
)

func AccrualDTOToCoin(dto AccrualDTO) (*domain.Coin, error) {
	reasonType, err := domain.NewTypeFromString(dto.reasonType)
	if err != nil {
		return nil, err
	}

	var textTransl map[sharedDomain.Lang]string
	if err := mapstructure.WeakDecode(dto.reasonTextTransaction, &textTransl); err != nil {
		return nil, fmt.Errorf("map translations struct failed: %s", err)
	}

	reason := domain.NewReason(
		dto.reasonID,
		*reasonType,
		textTransl,
		dto.appSlug,
	)

	return domain.NewCoin(
		*domain.NewCoinID(),
		dto.userID,
		dto.amount,
		dto.accruedAt,
		*reason,
	), nil
}
