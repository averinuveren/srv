package application

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"shop-service/internal/cmsuser/application/dto"
	"shop-service/internal/cmsuser/domain"
)

type CmsUserService interface {
	GetByID(id uint) (*domain.CmsUser, error)
	Login(dto dto.LoginDTO) (*domain.CmsUser, error)
	Update(dto dto.UpdateDTO) (*domain.CmsUser, error)
	Create(createDTO dto.CreateDTO) error
}

var (
	credentialInvalid = errors.New("password invalid")
)

type cmsUserServiceImpl struct {
	r domain.CmsUserRepository
}

func ProvideCmsUserService(r domain.CmsUserRepository) CmsUserService {
	return &cmsUserServiceImpl{r: r}
}

func (c *cmsUserServiceImpl) GetByID(id uint) (*domain.CmsUser, error) {
	u, err := c.r.GetByID(id)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (c *cmsUserServiceImpl) Login(dto dto.LoginDTO) (*domain.CmsUser, error) {
	cu, err := c.r.GetByEmail(dto.Email())
	if err != nil {
		return nil, credentialInvalid
	}
	if err := bcrypt.CompareHashAndPassword([]byte(cu.Password()), []byte(dto.Password())); err != nil {
		return nil, credentialInvalid
	}
	return cu, nil
}

func (c *cmsUserServiceImpl) Create(dto dto.CreateDTO) error {
	pass, _ := bcrypt.GenerateFromPassword([]byte(dto.Password), bcrypt.MinCost)
	if err := c.r.Store(dto.Name, dto.Email, string(pass)); err != nil {
		return err
	}
	return nil
}

func (c *cmsUserServiceImpl) Update(dto dto.UpdateDTO) (*domain.CmsUser, error) {
	cu, err := c.r.GetByID(dto.Id())
	if err != nil {
		return nil, err
	}
	if dto.Name() != nil {
		cu.SetName(*dto.Name())
	}
	if dto.Avatar() != nil {
		cu.SetAvatar(*dto.Avatar())
	}
	if dto.Email() != nil {
		cu.SetEmail(*dto.Email())
	}
	if dto.Password() != nil {
		pass, _ := bcrypt.GenerateFromPassword([]byte(*dto.Password()), bcrypt.MinCost)
		cu.SetPassword(string(pass))
	}
	if err := c.r.Update(*cu); err != nil {
		return nil, err
	}
	return cu, nil
}
