package domain

import "github.com/google/uuid"

type TicketID struct {
	id string
}

func NewTicketID() *TicketID {
	return &TicketID{id: uuid.New().String()}
}

func (t *TicketID) Value() string {
	return t.id
}

func MakeTicketIDFromString(s string) *TicketID {
	return &TicketID{id: s}
}
