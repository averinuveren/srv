package domain

type Image struct {
	id    ImageID
	lotID LotID
	image string
}

func NewImage(id ImageID, lotID LotID, image string) *Image {
	return &Image{id: id, lotID: lotID, image: image}
}

func (i *Image) ID() *ImageID {
	return &i.id
}

func (i *Image) LotID() LotID {
	return i.lotID
}

func (i *Image) Image() string {
	return i.image
}

func (i *Image) SetImage(image string) {
	i.image = image
}
