#!/usr/bin/env bash

if [ -f /shop-service/build/docker/app/.env ]; then
  # shellcheck disable=SC2002
  # shellcheck disable=SC2046
  export $(cat /shop-service/build/docker/app/.env | grep -v '#' | awk '/=/ {print $1}')
fi

cd /shop-service/cmd/shopservice || exit
rm srv >/dev/null 2>&1
echo "--> Building..."
go build -gcflags "all=-N -l" -o srv

if [ -f srv ]; then
  case $DEBUG in
  true)
    echo "--> Run debug"
    dlv --listen=:40000 --headless=true --api-version=2 --accept-multiclient exec ./srv
    ;;

  *)
    echo "--> Run srv"
    ./srv
    ;;
  esac
fi
