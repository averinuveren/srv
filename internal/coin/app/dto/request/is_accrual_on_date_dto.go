package request

import "time"

type IsAccrualOnDateDTO struct {
	UserID     uint
	ReasonType string
	Date       time.Time
	AppSlug    string
}

func NewIsAccrualOnDateDTO(
	userId uint,
	reasonType string,
	date time.Time,
	appSlug string,
) *IsAccrualOnDateDTO {
	return &IsAccrualOnDateDTO{
		UserID:     userId,
		ReasonType: reasonType,
		Date:       date,
		AppSlug:    appSlug,
	}
}
