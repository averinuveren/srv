package response

type DrawInfo struct {
	PrizesCount        uint `json:"prizes_count"`
	BoughtTicketsCount uint `json:"bought_tickets_count"`
}

type LotForUserDTO struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	Description   string    `json:"description"`
	Price         *uint     `json:"price"`
	Count         uint      `json:"count"`
	LeftoverCount uint      `json:"leftover_count"`
	ActiveAt      *string   `json:"active_at"`
	InactiveAt    *string   `json:"inactive_at"`
	LimitPerUser  *uint     `json:"limit_per_user"`
	Images        []string  `json:"images"`
	ProductType   string    `json:"product_type"`
	Draw          *DrawInfo `json:"draw"`
}

func NewLotForUserDTO(
	ID string,
	name string,
	description string,
	price *uint,
	count uint,
	leftoverCount uint,
	activeAt *string,
	inactiveAt *string,
	limitPerUser *uint,
	images []string,
	productType string,
) *LotForUserDTO {
	return &LotForUserDTO{
		ID:            ID,
		Name:          name,
		Description:   description,
		Price:         price,
		Count:         count,
		LeftoverCount: leftoverCount,
		ActiveAt:      activeAt,
		InactiveAt:    inactiveAt,
		LimitPerUser:  limitPerUser,
		Images:        images,
		ProductType:   productType,
	}
}

type LotForUserByPeriodDTO struct {
	Current  []*LotForUserDTO `json:"current"`
	Upcoming []*LotForUserDTO `json:"upcoming"`
	Past     []*LotForUserDTO `json:"past"`
}

func NewLotForUserByPeriodDTO(
	current []*LotForUserDTO,
	upcoming []*LotForUserDTO,
	past []*LotForUserDTO,
) *LotForUserByPeriodDTO {
	return &LotForUserByPeriodDTO{Current: current, Upcoming: upcoming, Past: past}
}
