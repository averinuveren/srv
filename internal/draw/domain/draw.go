package domain

import coreDomain "shop-service/internal/core/domain"

type Draw struct {
	id            DrawID
	lotID         coreDomain.LotID
	winningPlaces uint
	winners       []*Winner // TODO: make winners
}

func NewDraw(
	id DrawID,
	lotID coreDomain.LotID,
	winningPlaces uint,
	winners []*Winner,
) *Draw {
	return &Draw{
		id:            id,
		lotID:         lotID,
		winningPlaces: winningPlaces,
		winners:       winners,
	}
}

func (d *Draw) ID() *DrawID {
	return &d.id
}

func (d *Draw) LotID() *coreDomain.LotID {
	return &d.lotID
}

func (d *Draw) WinningPlaces() uint {
	return d.winningPlaces
}

func (d *Draw) Winners() []*Winner {
	return d.winners
}

func (d *Draw) ProductID() string {
	return d.id.Value()
}

func (d *Draw) SetWinningPlaces(winningPlaces uint) {
	d.winningPlaces = winningPlaces
}
