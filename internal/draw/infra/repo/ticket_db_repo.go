package repo

import (
	"context"
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"shop-service/internal/draw/domain"
	"shop-service/internal/shared/app"
)

const ticketTableName = "tickets"

type TicketDBRepo struct {
	db pgxtype.Querier
}

func ProvideTicketDBRepo(db *pgxpool.Pool) domain.TicketRepository {
	return &TicketDBRepo{db: db}
}

func (r *TicketDBRepo) Store(ctx context.Context, t domain.Ticket) error {
	sql := fmt.Sprintf(`insert into %s (id, draw_id, user_id, hash, used, created_at, updated_at)
		values ($1, $2, $3, $4, $5, now(), now())`, ticketTableName)
	_, err := r.db.Exec(ctx, sql,
		t.ID().Value(),
		t.DrawID().Value(),
		t.UserID(),
		t.Hash(),
		t.Used(),
	)
	if err != nil {
		return fmt.Errorf("failed exec insert: %s", err.Error())
	}
	return nil
}

func (r *TicketDBRepo) GetWhereUserID(ctx context.Context, userID uint) ([]*domain.Ticket, error) {
	sql := fmt.Sprintf("select %s from %s where user_id=$1", r.defaultSelect(), ticketTableName)
	rows, err := r.db.Query(ctx, sql, userID)
	if err != nil {
		return nil, fmt.Errorf("query failed: %s", err.Error())
	}
	defer rows.Close()
	var tickets []*domain.Ticket
	for rows.Next() {
		var (
			id     string
			drawID string
			hash   string
			used   bool
		)
		err = rows.Scan(&id, &drawID, nil, &hash, &used)
		if err != nil {
			return nil, fmt.Errorf("scan failed: %s", err.Error())
		}
		ticketID := domain.MakeTicketIDFromString(id)
		tickets = append(tickets, domain.NewTicket(*ticketID, *domain.NewDrawIDFromString(drawID), userID, hash, used))
	}
	return tickets, nil
}

func (r *TicketDBRepo) GetWhereUserIDAndDrawID(ctx context.Context, userID uint, drawID domain.DrawID) ([]*domain.Ticket, error) {
	sql := fmt.Sprintf("select %s from %s where user_id=$1 and draw_id=$2", r.defaultSelect(), ticketTableName)
	rows, err := r.db.Query(ctx, sql, userID, drawID.Value())
	if err != nil {
		return nil, fmt.Errorf("query failed: %s", err.Error())
	}
	defer rows.Close()
	var tickets []*domain.Ticket
	for rows.Next() {
		var (
			id   string
			hash string
			used bool
		)
		err = rows.Scan(&id, nil, nil, &hash, &used)
		if err != nil {
			return nil, fmt.Errorf("scan failed: %s", err.Error())
		}
		ticketID := domain.MakeTicketIDFromString(id)
		tickets = append(tickets, domain.NewTicket(*ticketID, drawID, userID, hash, used))
	}
	return tickets, nil
}

func (r TicketDBRepo) MakeFromTx(tx app.Tx) domain.TicketRepository {
	r.db = tx.Conn().(pgx.Tx)
	return &r
}

func (r *TicketDBRepo) defaultSelect() interface{} {
	return "id, draw_id, user_id, hash, used"
}
