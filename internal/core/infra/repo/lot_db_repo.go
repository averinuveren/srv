package repo

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	coreDomain "shop-service/internal/core/domain"
	drawDomain "shop-service/internal/draw/domain"
	"shop-service/internal/shared/app"
	sharedDomain "shop-service/internal/shared/domain"
	"shop-service/internal/shared/infra/ctxkey"
	"time"
)

const (
	lotTableName             = "lots"
	lotTranslationsTableName = "lot_translations"
	lotImagesTableName       = "lot_images"
)

type LotDBRepository struct {
	db       pgxtype.Querier
	drawRepo drawDomain.Repository
}

func ProvideLotDBRepository(
	db *pgxpool.Pool,
	drawRepo drawDomain.Repository,
) coreDomain.Repository {
	return &LotDBRepository{
		db:       db,
		drawRepo: drawRepo,
	}
}

func (r *LotDBRepository) GetAll(ctx context.Context) ([]*coreDomain.Lot, error) {
	sql := fmt.Sprintf(`select %s from %s`, r.defaultSelect(), lotTableName)
	rows, err := r.db.Query(ctx, sql)
	if err != nil {
		return nil, fmt.Errorf("lots query failed: %s", err.Error())
	}
	defer rows.Close()
	lots := make([]*coreDomain.Lot, 0)
	for rows.Next() {
		l, err := r.rowToLot(ctx, rows)
		if err != nil {
			return nil, err
		}
		lots = append(lots, l)
	}
	return lots, nil
}

func (r *LotDBRepository) Store(ctx context.Context, l coreDomain.Lot) error {
	lotID := l.ID().Value()
	sql := fmt.Sprintf(`insert into %s
		(id, price, count, leftover_count, active_at, inactive_at, limit_per_user, product_type, created_at, updated_at)
		values ($1, $2, $3, $4, $5, $6, $7, $8, now(), now())`, lotTableName)

	var activeAt *string
	if l.ActiveAt() != nil {
		activeAt = new(string)
		*activeAt = l.ActiveAt().Format(time.RFC3339)
	}

	var inactiveAt *string
	if l.InactiveAt() != nil {
		inactiveAt = new(string)
		*inactiveAt = l.InactiveAt().Format(time.RFC3339)
	}

	if _, err := r.db.Exec(ctx, sql,
		lotID,
		l.Price(),
		l.Count(),
		l.LeftoverCount(),
		activeAt,
		inactiveAt,
		l.LimitPerUser(),
		l.ProductType().String(),
	); err != nil {
		return err
	}

	for lang, t := range l.GetTranslations() {
		sql := fmt.Sprintf(`insert into %s
			(lot_id, locale, name, description, created_at, updated_at)
			values ($1, $2, $3, $4, now(), now())`, lotTranslationsTableName)
		if _, err := r.db.Exec(ctx, sql, lotID, lang, t[coreDomain.Name], t[coreDomain.Description]); err != nil {
			return err
		}
	}

	for _, i := range l.Images() {
		sql := fmt.Sprintf(`insert into %s (id, lot_id, image, created_at, updated_at)
			values ($1, $2, $3, now(), now())`, lotImagesTableName)
		if _, err := r.db.Exec(ctx, sql, uuid.New().String(), lotID, i.Image()); err != nil {
			return err
		}
	}

	return nil
}

func (r *LotDBRepository) FirstWereID(ctx context.Context, id coreDomain.LotID) (*coreDomain.Lot, error) {
	sql := fmt.Sprintf(`select %s from %s where id=$1`, r.defaultSelect(), lotTableName)
	row := r.db.QueryRow(ctx, sql, id.Value())
	return r.rowToLot(ctx, row)
}

func (r *LotDBRepository) Update(ctx context.Context, lot coreDomain.Lot) error {
	sql := fmt.Sprintf(`update %s
		set price=$1,
			count=$2,
			leftover_count=$3,
			active_at=$4,
			inactive_at=$5,
			limit_per_user=$6,
			updated_at=now()
			where id=$7`, lotTableName)

	var activeAt *string
	if lot.ActiveAt() != nil {
		activeAt = new(string)
		*activeAt = lot.ActiveAt().Format(time.RFC3339)
	}

	var inactiveAt *string
	if lot.InactiveAt() != nil {
		inactiveAt = new(string)
		*inactiveAt = lot.InactiveAt().Format(time.RFC3339)
	}

	if _, err := r.db.Exec(ctx, sql,
		lot.Price(),
		lot.Count(),
		lot.LeftoverCount(),
		activeAt,
		inactiveAt,
		lot.LimitPerUser(),
		lot.ID().Value(),
	); err != nil {
		return err
	}

	for lang, t := range lot.GetTranslations() {
		sql = fmt.Sprintf(`update %s set
			name=$1, description=$2, updated_at=now()
			where lot_id=$3 and locale=$4;`, lotTranslationsTableName)
		ct, err := r.db.Exec(ctx, sql, t[coreDomain.Name], t[coreDomain.Description], lot.ID().Value(), lang)
		if err != nil {
			return err
		}
		if ct.RowsAffected() == 0 {
			sql := fmt.Sprintf(`insert into %s
			(lot_id, locale, name, description, created_at, updated_at)
			values ($1, $2, $3, $4, now(), now())`, lotTranslationsTableName)
			if _, err := r.db.Exec(ctx, sql, lot.ID().Value(), lang, t[coreDomain.Name], t[coreDomain.Description]); err != nil {
				return err
			}
		}
	}

	sql = fmt.Sprintf(`delete from %s where lot_id=$1`, lotImagesTableName)
	if _, err := r.db.Exec(ctx, sql, lot.ID().Value()); err != nil {
		return err
	}

	for _, i := range lot.Images() {
		sql := fmt.Sprintf(`insert into %s (id, lot_id, image, created_at, updated_at)
			values ($1, $2, $3, now(), now())`, lotImagesTableName)
		if _, err := r.db.Exec(ctx, sql, i.ID().Value(), lot.ID().Value(), i.Image()); err != nil {
			return err
		}
	}

	return nil
}

func (r *LotDBRepository) Delete(ctx context.Context, lotID coreDomain.LotID) error {
	lotIDValue := lotID.Value()
	sql := fmt.Sprintf(`delete from %s where lot_id=$1`, lotImagesTableName)
	if _, err := r.db.Exec(ctx, sql, lotIDValue); err != nil {
		return err
	}

	sql = fmt.Sprintf(`delete from %s where lot_id=$1`, lotTranslationsTableName)
	if _, err := r.db.Exec(ctx, sql, lotIDValue); err != nil {
		return err
	}

	sql = fmt.Sprintf(`delete from %s where id=$1`, lotTableName)
	if _, err := r.db.Exec(ctx, sql, lotIDValue); err != nil {
		return err
	}

	return nil
}

func (r *LotDBRepository) GetWhereTimeBetweenActiveAndInactive(ctx context.Context, t time.Time) ([]*coreDomain.Lot, error) {
	sql := fmt.Sprintf(`select %s from %s
		where $1::timestamp between active_at and inactive_at`, r.defaultSelect(), lotTableName)
	rows, err := r.db.Query(ctx, sql, t.Format(time.RFC3339))
	if err != nil {
		return nil, fmt.Errorf("query failed: %s", err.Error())
	}
	defer rows.Close()
	lots := make([]*coreDomain.Lot, 0)
	for rows.Next() {
		l, err := r.rowToLot(ctx, rows)
		if err != nil {
			return nil, err
		}
		lots = append(lots, l)
	}
	return lots, nil
}

func (r LotDBRepository) MakeFromTx(tx app.Tx) coreDomain.Repository {
	r.db = tx.Conn().(pgx.Tx)
	return &r
}

func (r *LotDBRepository) defaultSelect() string {
	return "id, price, count, leftover_count, active_at, inactive_at, limit_per_user, product_type"
}

func (r *LotDBRepository) rowToLot(ctx context.Context, row pgx.Row) (*coreDomain.Lot, error) {
	var (
		lotID         string
		price         *uint
		count         uint
		leftoverCount uint
		activeAt      *time.Time
		inactiveAt    *time.Time
		limitPerUser  *uint
		productType   string
	)
	err := row.Scan(&lotID, &price, &count, &leftoverCount, &activeAt, &inactiveAt, &limitPerUser, &productType)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, coreDomain.NofFound
		}
		return nil, fmt.Errorf("lot scan failed: %s", err.Error())
	}

	translSql := fmt.Sprintf(`select
		locale, name, description from %s where lot_id=$1`, lotTranslationsTableName)
	translRows, err := r.db.Query(ctx, translSql, lotID)
	if err != nil {
		return nil, fmt.Errorf("lot_translations query failed: %s", err.Error())
	}
	defer translRows.Close()
	transl := make(map[sharedDomain.Lang]map[sharedDomain.TranslationField]*string)
	for translRows.Next() {
		var locale, name, description string
		if err := translRows.Scan(&locale, &name, &description); err != nil {
			return nil, fmt.Errorf("lot_translations scan failed: %s", err.Error())
		}
		transl[sharedDomain.Lang(locale)] = map[sharedDomain.TranslationField]*string{
			coreDomain.Name:        &name,
			coreDomain.Description: &description,
		}
	}

	iSql := fmt.Sprintf(`select id, image from %s where lot_id=$1`, lotImagesTableName)
	iRows, err := r.db.Query(ctx, iSql, lotID)
	if err != nil {
		return nil, fmt.Errorf("lot_images query failed: %s", err.Error())
	}
	defer iRows.Close()
	lotIDvo := coreDomain.NewLotIDFromString(lotID)
	images := make([]*coreDomain.Image, 0)
	for iRows.Next() {
		var imageID, image string
		if err := iRows.Scan(&imageID, &image); err != nil {
			return nil, fmt.Errorf("lot_images scan failed: %s", err.Error())
		}
		images = append(images, coreDomain.NewImage(
			*coreDomain.NewImageIDFromString(imageID),
			*lotIDvo,
			image,
		))
	}

	pt, err := coreDomain.NewProductTypeFromString(productType)
	if err != nil {
		return nil, err
	}

	var product coreDomain.Product
	switch *pt {
	case coreDomain.ProductTypeDraw:
		product, err = r.drawRepo.FirstWereLotID(ctx, *lotIDvo)
		if err != nil {
			return nil, fmt.Errorf("failed first draw by lot id %s: %s", lotIDvo.Value(), err)
		}
	}

	lang := ctxkey.GetLang(ctx)

	return coreDomain.NewLot(
		*lotIDvo,
		*transl[sharedDomain.Lang(lang)][coreDomain.Name],
		*transl[sharedDomain.Lang(lang)][coreDomain.Description],
		price,
		count,
		leftoverCount,
		activeAt,
		inactiveAt,
		limitPerUser,
		images,
		*pt,
		product,
		transl,
		sharedDomain.Lang(lang),
	), nil
}
