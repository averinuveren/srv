package domain

import "github.com/google/uuid"

type LotID struct {
	id string
}

func NewLotID() *LotID {
	return &LotID{uuid.New().String()}
}

func NewLotIDFromString(id string) *LotID {
	return &LotID{id}
}

func (id *LotID) Value() string {
	return id.id
}
