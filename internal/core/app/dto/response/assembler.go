package response

import (
	"shop-service/internal/core/domain"
	drawDomain "shop-service/internal/draw/domain"
	"shop-service/internal/shared/infra/cloudstorage"
	"time"
)

func EntityToLotDTO(lot domain.Lot) *LotDTO {
	images := make([]*Image, len(lot.Images()))
	for i, image := range lot.Images() {
		path := image.Image()
		images[i] = NewImage(image.ID().Value(), *cloudstorage.Instance().Url(&path))
	}

	transl := make(map[string]map[string]*string)
	for lang, tr := range lot.GetTranslations() {
		transl[string(lang)] = map[string]*string{
			domain.Name:        tr[domain.Name],
			domain.Description: tr[domain.Description],
		}
	}

	var draw *Draw

	switch lot.ProductType() {
	case domain.ProductTypeDraw:
		t := lot.Product().(*drawDomain.Draw)
		draw = NewDraw(t.WinningPlaces())
	}

	var activeAt *string
	if lot.ActiveAt() != nil {
		activeAt = new(string)
		*activeAt = lot.ActiveAt().Format(time.RFC3339)
	}

	var inactiveAt *string
	if lot.ActiveAt() != nil {
		inactiveAt = new(string)
		*inactiveAt = lot.ActiveAt().Format(time.RFC3339)
	}

	return NewLotDTO(
		lot.ID().Value(),
		lot.Name(),
		lot.Description(),
		lot.Price(),
		lot.Count(),
		lot.LeftoverCount(),
		activeAt,
		inactiveAt,
		lot.LimitPerUser(),
		images,
		lot.ProductType().String(),
		draw,
		transl,
	)
}

func EntityToLotForUserDTO(lot domain.Lot) *LotForUserDTO {
	images := make([]string, len(lot.Images()))
	for i, image := range lot.Images() {
		path := image.Image()
		images[i] = *cloudstorage.Instance().Url(&path)
	}

	transl := make(map[string]map[string]*string)
	for lang, tr := range lot.GetTranslations() {
		transl[string(lang)] = map[string]*string{
			domain.Name:        tr[domain.Name],
			domain.Description: tr[domain.Description],
		}
	}

	var activeAt *string
	if lot.ActiveAt() != nil {
		activeAt = new(string)
		*activeAt = lot.ActiveAt().Format(time.RFC3339)
	}

	var inactiveAt *string
	if lot.ActiveAt() != nil {
		inactiveAt = new(string)
		*inactiveAt = lot.InactiveAt().Format(time.RFC3339)
	}

	return NewLotForUserDTO(
		lot.ID().Value(),
		lot.Name(),
		lot.Description(),
		lot.Price(),
		lot.Count(),
		lot.LeftoverCount(),
		activeAt,
		inactiveAt,
		lot.LimitPerUser(),
		images,
		lot.ProductType().String(),
	)
}
