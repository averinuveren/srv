package domain

type Winner struct {
	id WinnerID
}

func (w *Winner) Id() WinnerID {
	return w.id
}
