package dto

type UpdateDTO struct {
	id       uint
	name     *string
	avatar   *string
	email    *string
	password *string
}

func NewUpdateDTO(id uint, name *string, avatar *string, email *string, password *string) *UpdateDTO {
	return &UpdateDTO{id: id, name: name, avatar: avatar, email: email, password: password}
}

func (u *UpdateDTO) Id() uint {
	return u.id
}

func (u UpdateDTO) Name() *string {
	return u.name
}

func (u *UpdateDTO) Avatar() *string {
	return u.avatar
}

func (u UpdateDTO) Email() *string {
	return u.email
}

func (u *UpdateDTO) Password() *string {
	return u.password
}
