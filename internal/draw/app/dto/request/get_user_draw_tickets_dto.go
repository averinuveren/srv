package request

type GetUserDrawTicketsDTO struct {
	userID uint
	drawID string
}

func NewGetUserDrawTicketsDTO(
	userID uint,
	drawID string,
) *GetUserDrawTicketsDTO {
	return &GetUserDrawTicketsDTO{
		userID: userID,
		drawID: drawID,
	}
}

func (g *GetUserDrawTicketsDTO) UserID() uint {
	return g.userID
}

func (g *GetUserDrawTicketsDTO) DrawID() string {
	return g.drawID
}
