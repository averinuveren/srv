package app

import (
	"context"
	"errors"
	"fmt"
	"shop-service/internal/coin/app/dto/request"
	"shop-service/internal/coin/app/dto/response"
	sharedApp "shop-service/internal/shared/app"
	"time"

	"shop-service/internal/coin/domain"
	sharedDomain "shop-service/internal/shared/domain"
	"shop-service/internal/shared/infra/ctxkey"
)

type Application interface {
	Accrual(ctx context.Context, dto request.AccrualDTO) error
	AccrualMany(ctx context.Context, dto []request.AccrualDTO) error
	GetUserCoinAmount(ctx context.Context, userID uint) (*int, error)
	GetUserWhereReason(ctx context.Context, dto request.GetUserWhereReasonDTO) ([]*domain.Coin, error) // TODO: coin DTO
	UserEarnings(ctx context.Context, userID uint) (*int, error)
	UserSpent(ctx context.Context, userID uint) (*int, error)
	UserHistory(ctx context.Context, userID uint) ([]response.UserHistoryDTO, error)
	IsAccrualOnDate(ctx context.Context, dto request.IsAccrualOnDateDTO) (*bool, error)
}

type applicationImpl struct {
	r         domain.Repository
	txManager sharedApp.TxManager
}

func ProvideApplicationImpl(
	r domain.Repository,
	txManager sharedApp.TxManager,
) Application {
	return &applicationImpl{
		r:         r,
		txManager: txManager,
	}
}

func (a *applicationImpl) Accrual(ctx context.Context, dto request.AccrualDTO) error {
	coin, err := request.AccrualDTOToCoin(dto)
	if err != nil {
		return fmt.Errorf("dto to coin conversion failed: %s", err)
	}
	return a.r.Store(ctx, *coin)
}

func (a *applicationImpl) AccrualMany(ctx context.Context, dto []request.AccrualDTO) error {
	tx, err := a.txManager.Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin transaction failed: %s", err.Error())
	}
	defer tx.Rollback(ctx)

	txRepo := a.r.MakeFromTx(tx)

	for _, d := range dto {
		coin, err := request.AccrualDTOToCoin(d)
		if err != nil {
			return fmt.Errorf("dto to coin conversion failed: %s", err)
		}
		err = txRepo.Store(ctx, *coin)
		if err != nil {
			return fmt.Errorf("store failed: %s", err.Error())
		}
	}
	return nil
}

func (a *applicationImpl) GetUserCoinAmount(ctx context.Context, userID uint) (*int, error) {
	coins, err := a.r.GetByUserID(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("get all by user id failed: %v", err)
	}

	var amount int
	for _, c := range coins {
		amount += c.Amount()
	}

	return &amount, nil
}

func (a *applicationImpl) GetUserWhereReason(ctx context.Context, dto request.GetUserWhereReasonDTO) ([]*domain.Coin, error) {
	reason := domain.NewReason(dto.ReasonID(), dto.ReasonType(), nil, "")
	return a.r.GetWhereUserIDAndReason(ctx, dto.UserID(), *reason)
}

func (a *applicationImpl) UserEarnings(ctx context.Context, userID uint) (*int, error) {
	coins, err := a.r.GetWhereUserIDAndAmountPositive(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("get where user id and amount positive failed: %s", err.Error())
	}
	var earnings int
	for _, c := range coins {
		earnings += c.Amount()
	}
	return &earnings, nil
}

func (a *applicationImpl) UserSpent(ctx context.Context, userID uint) (*int, error) {
	coins, err := a.r.GetWhereUserIDAndAmountNegative(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("get where user id and amount positive failed: %s", err.Error())
	}
	var spent int
	for _, c := range coins {
		spent += c.Amount()
	}
	return &spent, nil
}

func (a *applicationImpl) UserHistory(ctx context.Context, userID uint) ([]response.UserHistoryDTO, error) {
	coins, err := a.r.GetByUserID(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("get by userID failed: %s", err.Error())
	}

	var userHistoriesDTO []response.UserHistoryDTO

	lang := ctxkey.GetLang(ctx)

	for _, c := range coins {
		userHistoriesDTO = append(userHistoriesDTO, response.UserHistoryDTO{
			ID:         c.ID().Value(),
			ReasonText: c.Reason().TextTranslations()[sharedDomain.Lang(lang)],
			AccruedAt:  c.AccruedAt().Format(time.RFC3339),
			Amount:     int32(c.Amount()),
		})
	}

	return userHistoriesDTO, nil
}

func (a *applicationImpl) IsAccrualOnDate(ctx context.Context, dto request.IsAccrualOnDateDTO) (*bool, error) {
	var res bool

	_, err := a.r.FirstWhereUserIDReasonTypeDateAppSlug(ctx, dto.UserID, dto.ReasonType, dto.Date, dto.AppSlug)
	if err != nil {
		if errors.Is(err, domain.NotFound) {
			return &res, nil
		}
		return nil, fmt.Errorf("first where userID reasonType date and appSlug failed: %s", err.Error())
	}

	res = true

	return &res, nil
}
