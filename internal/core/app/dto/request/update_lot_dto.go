package request

import "time"

type UpdateLotDTO struct {
	Id           string
	Price        *uint
	Count        uint
	ActiveAt     *time.Time
	InactiveAt   *time.Time
	LimitPerUser *uint
	Images       []*Image
	Draw         *Draw
	Translations map[Lang]*LotTransl
}

func NewUpdateLotDTO(
	id string,
	price *uint,
	count uint,
	activeAt *time.Time,
	inactiveAt *time.Time,
	limitPerUser *uint,
	images []*Image,
	draw *Draw,
	translations map[Lang]*LotTransl,
) *UpdateLotDTO {
	return &UpdateLotDTO{
		Id:           id,
		Price:        price,
		Count:        count,
		ActiveAt:     activeAt,
		InactiveAt:   inactiveAt,
		LimitPerUser: limitPerUser,
		Images:       images,
		Draw:         draw,
		Translations: translations,
	}
}
