package inputs

type LotTranslations struct {
	Name        string `mapstructure:"name" validate:"required"`
	Description string `mapstructure:"description" validate:"required"`
}

type Draw struct {
	WinningPlaces uint `mapstructure:"winning_places" validate:"required"`
}
