package domain

import "github.com/google/uuid"

type Ticket struct {
	id     TicketID
	drawID DrawID
	userID uint
	hash   string
	used   bool
}

func NewTicket(id TicketID, drawID DrawID, userID uint, hash string, used bool) *Ticket {
	return &Ticket{id: id, drawID: drawID, userID: userID, hash: hash, used: used}
}

func (t *Ticket) ID() *TicketID {
	return &t.id
}

func (t *Ticket) DrawID() *DrawID {
	return &t.drawID
}

func (t *Ticket) UserID() uint {
	return t.userID
}

func (t *Ticket) Hash() string {
	return t.hash
}

func (t *Ticket) Used() bool {
	return t.used
}

func MakeTicketWithHash(drawID DrawID, userID uint) *Ticket {
	return &Ticket{
		id:     *NewTicketID(),
		drawID: drawID,
		userID: userID,
		hash:   uuid.New().String()[:8],
	}
}
