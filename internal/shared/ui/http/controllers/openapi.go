package controllers

import (
	"io/ioutil"
	"net/http"
	"os"
)

type OpenAPIController struct {
}

func ProvideOpenAPIController() *OpenAPIController {
	return &OpenAPIController{}
}

func (_ *OpenAPIController) Index(w http.ResponseWriter, _ *http.Request) {
	f, err := os.Open("/shop-service/internal/shared/ui/http/resources/views/openapi.html")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	b, _ := ioutil.ReadAll(f)
	_, _ = w.Write(b)
}

func (_ *OpenAPIController) Yaml(w http.ResponseWriter, _ *http.Request) {
	f, err := os.Open("/shop-service/internal/shared/ui/http/resources/openapi/build/openapi.yaml")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	b, _ := ioutil.ReadAll(f)
	_, _ = w.Write(b)
}
