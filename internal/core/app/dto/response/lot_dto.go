package response

type LotDTO struct {
	ID            string                        `json:"id"`
	Name          string                        `json:"name"`
	Description   string                        `json:"description"`
	Price         *uint                         `json:"price"`
	Count         uint                          `json:"count"`
	LeftoverCount uint                          `json:"leftover_count"`
	ActiveAt      *string                       `json:"active_at"`
	InactiveAt    *string                       `json:"inactive_at"`
	LimitPerUser  *uint                         `json:"limit_per_user"`
	Images        []*Image                      `json:"images"`
	ProductType   string                        `json:"product_type"`
	Draw          *Draw                         `json:"draw"`
	Translations  map[string]map[string]*string `json:"translations"`
}

func NewLotDTO(
	id string,
	name string,
	description string,
	price *uint,
	count uint,
	leftoverCount uint,
	activeAt *string,
	inactiveAt *string,
	limitPerUser *uint,
	images []*Image,
	productType string,
	draw *Draw,
	translations map[string]map[string]*string,
) *LotDTO {
	return &LotDTO{
		ID:            id,
		Name:          name,
		Description:   description,
		Price:         price,
		Count:         count,
		LeftoverCount: leftoverCount,
		ActiveAt:      activeAt,
		InactiveAt:    inactiveAt,
		LimitPerUser:  limitPerUser,
		Images:        images,
		ProductType:   productType,
		Draw:          draw,
		Translations:  translations,
	}
}
