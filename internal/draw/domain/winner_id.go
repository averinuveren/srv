package domain

import "github.com/google/uuid"

type WinnerID struct {
	id string
}

func NewWinnerID() *WinnerID {
	return &WinnerID{id: uuid.New().String()}
}

func (w *WinnerID) Value() string {
	return w.id
}
