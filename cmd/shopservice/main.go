package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
	"log"
	"net"
	"net/http"
	"shop-service/internal/shared/infra/ctxkey"
	"shop-service/internal/shared/infra/database"
	pb "shop-service/internal/shared/ui/rpc/protocol"
	"shop-service/internal/shared/ui/rpc/protocol/healthcheck"
	"sync"
)

func main() {
	db := database.Instance()
	defer db.Close()

	wg := new(sync.WaitGroup)
	wg.Add(2)

	go func() {
		lis, err := net.Listen("tcp4", "0.0.0.0:50051")
		if err != nil {
			log.Fatal(err.Error())
		}

		grpcServer := grpc.NewServer(
			grpc.UnaryInterceptor(
				func(
					ctx context.Context,
					req interface{},
					info *grpc.UnaryServerInfo,
					handler grpc.UnaryHandler,
				) (resp interface{}, err error) {
					md, ok := metadata.FromIncomingContext(ctx)
					if ok {
						if lang, ok := md["lang"]; ok && len(lang) != 0 {
							ctx = context.WithValue(ctx, ctxkey.ContextKeyLang, lang[0])
						} else {
							ctx = context.WithValue(ctx, ctxkey.ContextKeyLang, "ru")
						}
					}
					return handler(ctx, req)
				},
			),
		)
		pb.RegisterShopServiceServer(grpcServer, InitializeRpcService(db))

		healthService := healthcheck.NewHealthChecker()
		grpc_health_v1.RegisterHealthServer(grpcServer, healthService)

		fmt.Println(grpcServer.Serve(lis))
	}()

	go func() {
		fmt.Println(http.ListenAndServe(":80", InitializeRoute(db)))
		wg.Done()
	}()

	wg.Wait()
}
