dir := ${CURDIR}

help: ## show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build: ## build dev containers
	cd deployments/docker-compose && docker-compose build

up: ## start dev containers
	cd deployments/docker-compose && docker-compose up -d

build-prod: ## build prod containers
	cd deployments/docker-compose && docker-compose -f docker-compose.yml -f docker-compose.prod.yml build

up-prod: ## start prod containers
	cd deployments/docker-compose && docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

log: ## Show docker logs
	cd deployments/docker-compose && docker-compose logs --follow

restart: down up ## Restart all dev containers

down: ## stop containers
	cd deployments/docker-compose && docker-compose down -v

proto: ## gen proto file
	docker run -v "$(dir)/internal/shared/ui/rpc/protocol":/tmp/ -w /tmp znly/protoc \
        ./shop.proto \
        --go_out=plugins=grpc:./ \
        --proto_path=./

migration: ## make migration
	cd ./internal/shared/infra/database && echo "table name: " && read -r t && migrate create -ext sql -dir sql -seq $$t
