package cloudstorage

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"sync"
)

type CloudStorage interface {
	Upload(path string, buf *bytes.Buffer) error
	Url(path *string) *string
}

type cloudStorageImpl struct {
}

func newCloudStorageImpl() *cloudStorageImpl {
	return &cloudStorageImpl{}
}

func (c *cloudStorageImpl) Upload(path string, buf *bytes.Buffer) error {
	url := fmt.Sprintf("%s/%s", os.Getenv("BUNNYCDN_API_URL"), path)
	r, err := http.NewRequest(http.MethodPut, url, buf)
	if err != nil {
		return err
	}
	r.Header.Set("AccessKey", os.Getenv("BUNNYCDN_KEY"))
	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func (c *cloudStorageImpl) Url(path *string) *string {
	if path == nil {
		return nil
	}
	url := fmt.Sprintf("%s/%s", os.Getenv("BUNNYCDN_URL"), *path)
	return &url
}

var (
	instance CloudStorage
	once     sync.Once
)

func Instance() CloudStorage {
	once.Do(func() {
		instance = newCloudStorageImpl()
	})
	return instance
}
