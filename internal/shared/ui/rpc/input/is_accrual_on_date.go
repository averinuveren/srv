package input

import (
	"shop-service/internal/coin/app/dto/request"
	"time"
)

type IsAccrualOnDateInput struct {
	UserID     int32  `json:"user_id" validate:"required"`
	ReasonType string `json:"reason_type" validate:"required"`
	Date       string `json:"date" validate:"required,datetime=2006-01-02T15:04:05Z07:00"`
	AppSlug    string `json:"app_slug" validate:"required"`
}

func (i *IsAccrualOnDateInput) ToDTO() *request.IsAccrualOnDateDTO {
	date, _ := time.Parse(time.RFC3339, i.Date)

	return request.NewIsAccrualOnDateDTO(
		uint(i.UserID),
		i.ReasonType,
		date,
		i.AppSlug,
	)
}
