package domain

import (
	"shop-service/internal/shared/domain"
	"time"
)

const (
	Name        = "name"
	Description = "description"
)

type Lot struct {
	domain.AbstractMultilangEntity
	id            LotID
	name          string // multilang
	description   string // multilang
	price         *uint
	count         uint
	leftoverCount uint
	activeAt      *time.Time
	inactiveAt    *time.Time
	limitPerUser  *uint
	images        []*Image
	productType   ProductType
	product       Product
}

func NewLot(
	id LotID,
	name string,
	description string,
	price *uint,
	count uint,
	leftoverCount uint,
	activeAt *time.Time,
	inactiveAt *time.Time,
	limitPerUser *uint,
	images []*Image,
	productType ProductType,
	product Product,
	translations map[domain.Lang]map[domain.TranslationField]*string,
	lang domain.Lang,
) *Lot {
	ml := domain.NewAbstractMultilangEntity(lang, translations)
	return &Lot{
		AbstractMultilangEntity: *ml,
		id:                      id,
		name:                    name,
		description:             description,
		price:                   price,
		count:                   count,
		leftoverCount:           leftoverCount,
		activeAt:                activeAt,
		inactiveAt:              inactiveAt,
		limitPerUser:            limitPerUser,
		images:                  images,
		product:                 product,
		productType:             productType,
	}
}

func (l *Lot) ID() *LotID {
	return &l.id
}

func (l *Lot) Name() string {
	return l.name
}

func (l *Lot) Description() string {
	return l.description
}

func (l *Lot) Price() *uint {
	return l.price
}

func (l *Lot) Count() uint {
	return l.count
}

func (l *Lot) LeftoverCount() uint {
	return l.leftoverCount
}

func (l *Lot) ActiveAt() *time.Time {
	return l.activeAt
}

func (l *Lot) InactiveAt() *time.Time {
	return l.inactiveAt
}

func (l *Lot) LimitPerUser() *uint {
	return l.limitPerUser
}

func (l *Lot) Images() []*Image {
	return l.images
}

func (l *Lot) ProductType() ProductType {
	return l.productType
}

func (l *Lot) Product() Product {
	return l.product
}

func (l *Lot) SetPrice(price *uint) {
	l.price = price
}

func (l *Lot) SetCount(count uint) {
	l.count = count
}

func (l *Lot) SetLeftoverCount(leftoverCount uint) {
	l.leftoverCount = leftoverCount
}

func (l *Lot) SetActiveAt(activeAt *time.Time) {
	l.activeAt = activeAt
}

func (l *Lot) SetInactiveAt(inactiveAt *time.Time) {
	l.inactiveAt = inactiveAt
}

func (l *Lot) SetLimitPerUser(limitPerUser *uint) {
	l.limitPerUser = limitPerUser
}

func (l *Lot) SetImages(images []*Image) {
	l.images = images
}

func (l *Lot) SetProduct(product Product) {
	l.product = product
}

func (l *Lot) GetTranslationFields() []domain.TranslationField {
	return []domain.TranslationField{
		Name,
		Description,
	}
}

func (l *Lot) IsActive() bool {
	return l.ActiveAt().Unix() < time.Now().Unix() &&
		l.InactiveAt().Unix() > time.Now().Unix()
}
