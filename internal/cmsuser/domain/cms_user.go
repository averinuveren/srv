package domain

type CmsUser struct {
	id       uint
	name     string
	email    string
	password string
	avatar   string
}

func NewCmsUser(id uint, name string, email string, password string, avatar string) *CmsUser {
	return &CmsUser{id: id, name: name, email: email, password: password, avatar: avatar}
}

func (c *CmsUser) Id() uint {
	return c.id
}

func (c *CmsUser) Name() string {
	return c.name
}

func (c *CmsUser) Email() string {
	return c.email
}

func (c *CmsUser) Password() string {
	return c.password
}

func (c *CmsUser) Avatar() string {
	return c.avatar
}

func (c *CmsUser) SetName(name string) {
	c.name = name
}

func (c *CmsUser) SetEmail(email string) {
	c.email = email
}

func (c *CmsUser) SetPassword(password string) {
	c.password = password
}

func (c *CmsUser) SetAvatar(avatar string) {
	c.avatar = avatar
}
