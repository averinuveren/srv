create table if not exists tickets
(
    id         uuid primary key,
    draw_id    uuid        not null,
    user_id    int         not null,
    hash       varchar(50) not null,
    used       boolean     not null,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_tickets_draw_id
        FOREIGN KEY (draw_id)
            REFERENCES draws (id)
);
