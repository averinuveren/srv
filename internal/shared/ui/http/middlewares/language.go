package middlewares

import (
	"context"
	"net/http"
	"shop-service/internal/shared/infra/ctxkey"
)

func Language(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), ctxkey.ContextKeyLang, "ru")
		next.ServeHTTP(rw, r.WithContext(ctx))
	})
}
