package inputs

import (
	"mime/multipart"
	dtoRequest "shop-service/internal/core/app/dto/request"
	"time"
)

type ImageUpdate struct {
	ID        *string               `mapstructure:"id"`
	Image     *multipart.FileHeader `mapstructure:"image"`
	ImagePath *string
}

type UpdateLotInput struct {
	ID           string
	Price        *uint                       `mapstructure:"price"`
	Count        uint                        `mapstructure:"count"`
	ActiveAt     *string                     `mapstructure:"active_at" validate:"omitempty,datetime=2006-01-02 15:04"`
	InactiveAt   *string                     `mapstructure:"inactive_at" validate:"omitempty,datetime=2006-01-02 15:04"`
	LimitPerUser *uint                       `mapstructure:"limit_per_user"`
	Images       map[uint]*ImageUpdate       `mapstructure:"images" validate:"required,dive"`
	ProductType  string                      `mapstructure:"product_type" validate:"required"`
	Draw         *Draw                       `mapstructure:"draw" validate:"required_if=ProductType draw,dive"`
	Translations map[string]*LotTranslations `mapstructure:"translations" validate:"required,min=1,dive,lot-translations"`
}

func (i *UpdateLotInput) ToUpdateLotDTO() (*dtoRequest.UpdateLotDTO, error) {
	var err error

	var activeAt *time.Time
	if i.ActiveAt != nil {
		activeAt = new(time.Time)
		*activeAt, err = time.Parse("2006-01-02 15:04", *i.ActiveAt)
		if err != nil {
			return nil, err
		}
	}

	var inactiveAt *time.Time
	if i.InactiveAt != nil {
		inactiveAt = new(time.Time)
		*inactiveAt, err = time.Parse("2006-01-02 15:04", *i.InactiveAt)
		if err != nil {
			return nil, err
		}
	}

	images := make([]*dtoRequest.Image, len(i.Images))
	for index, v := range i.Images {
		images[index] = dtoRequest.NewImage(v.ID, v.ImagePath)
	}

	transl := make(map[dtoRequest.Lang]*dtoRequest.LotTransl)
	for lang, t := range i.Translations {
		transl[dtoRequest.Lang(lang)] = dtoRequest.NewLotTransl(t.Name, t.Description)
	}

	var draw *dtoRequest.Draw
	if i.Draw != nil {
		draw = dtoRequest.NewDraw(i.Draw.WinningPlaces)
	}

	return dtoRequest.NewUpdateLotDTO(
		i.ID,
		i.Price,
		i.Count,
		activeAt,
		inactiveAt,
		i.LimitPerUser,
		images,
		draw,
		transl,
	), nil
}
