package transactionmanager

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"shop-service/internal/shared/app"
)

type TxDB struct {
	tx pgx.Tx
}

func NewTxDB(tx pgx.Tx) *TxDB {
	return &TxDB{tx: tx}
}

func (t *TxDB) Commit(ctx context.Context) error {
	return t.tx.Commit(ctx)
}

func (t *TxDB) Rollback(ctx context.Context) {
	if err := t.tx.Rollback(ctx); err != nil {
		fmt.Println(err.Error())
	}
}

func (t *TxDB) Conn() interface{} {
	return t.tx
}

type TxManagerDB struct {
	db *pgxpool.Pool
}

func ProvideTxManagerDB(db *pgxpool.Pool) app.TxManager {
	return &TxManagerDB{db: db}
}

func (m *TxManagerDB) Begin(ctx context.Context) (app.Tx, error) {
	tx, err := m.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	return NewTxDB(tx), nil
}
