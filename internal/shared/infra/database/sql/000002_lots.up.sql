create table if not exists lots
(
    id             uuid primary key,
    price          integer,
    count          integer      not null,
    leftover_count integer      not null,
    active_at      timestamp,
    inactive_at    timestamp,
    limit_per_user integer,
    product_type   varchar(100) not null,
    created_at     timestamp,
    updated_at     timestamp
);
