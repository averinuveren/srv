package domain

import "fmt"

var types = []string{
	"draw",
}

type ProductType uint

func NewProductTypeFromString(s string) (*ProductType, error) {
	for i, v := range types {
		if v == s {
			t := ProductType(i)
			return &t, nil
		}
	}
	return nil, fmt.Errorf("couldn't create product type from '%s'", s)
}

func (pt ProductType) String() string {
	return types[pt]
}

const (
	ProductTypeDraw ProductType = iota
)
