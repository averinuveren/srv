package database

import (
	"context"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/pgx/v4/stdlib"
	"log"
	"os"
	"sync"
)

func makePool() *pgxpool.Pool {
	user := os.Getenv("PG_USER")
	pass := os.Getenv("PG_PASS")
	host := os.Getenv("PG_HOST")
	port := os.Getenv("PG_PORT")
	db := os.Getenv("PG_DB")
	connString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, db)
	dbpool, err := pgxpool.Connect(context.Background(), connString)
	if err != nil {
		log.Fatalf("Unable to connect to database: %v\n", err)
	}
	migrateUp(dbpool.Config())
	return dbpool
}

func migrateUp(c *pgxpool.Config) {
	cc := *c.ConnConfig
	driver, err := postgres.WithInstance(stdlib.OpenDB(cc), &postgres.Config{})
	if err != nil {
		panic(err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://sql", cc.Database, driver)
	if err != nil {
		panic(err)
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		panic(fmt.Sprintf("An error occurred while syncing the database.. %v", err))
	}
}

var (
	instance *pgxpool.Pool
	once     sync.Once
)

func Instance() *pgxpool.Pool {
	once.Do(func() {
		instance = makePool()
	})
	return instance
}
