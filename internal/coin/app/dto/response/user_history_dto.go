package response

type UserHistoryDTO struct {
	ID         string
	ReasonText string
	AccruedAt  string
	Amount     int32
}
