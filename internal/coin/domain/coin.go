package domain

import "time"

type Coin struct {
	id        CoinID
	userID    uint
	amount    int
	accruedAt time.Time
	reason    Reason
}

func NewCoin(
	id CoinID,
	userID uint,
	amount int,
	accruedAt time.Time,
	reason Reason,
) *Coin {
	return &Coin{
		id:        id,
		userID:    userID,
		amount:    amount,
		accruedAt: accruedAt,
		reason:    reason,
	}
}

func (c *Coin) ID() *CoinID {
	return &c.id
}

func (c *Coin) UserID() uint {
	return c.userID
}

func (c *Coin) Amount() int {
	return c.amount
}

func (c *Coin) AccruedAt() time.Time {
	return c.accruedAt
}

func (c *Coin) Reason() *Reason {
	return &c.reason
}
