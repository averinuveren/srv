package domain

import (
	"context"
	"shop-service/internal/shared/infra/transactionmanager"
	"time"
)

type Repository interface {
	GetByUserID(ctx context.Context, userID uint) ([]*Coin, error)
	Store(ctx context.Context, coin Coin) error
	GetWhereUserIDAndReason(ctx context.Context, userID uint, reason Reason) ([]*Coin, error)
	GetWhereUserIDAndAmountPositive(ctx context.Context, userID uint) ([]*Coin, error)
	GetWhereUserIDAndAmountNegative(ctx context.Context, userID uint) ([]*Coin, error)
	MakeFromTx(tx transactionmanager.Tx) Repository
	FirstWhereUserIDReasonTypeDateAppSlug(
		ctx context.Context,
		userID uint,
		reasonType string,
		date time.Time,
		appSlug string,
	) (*Coin, error)
}
