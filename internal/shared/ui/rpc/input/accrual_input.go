package input

import (
	"fmt"
	"shop-service/internal/coin/app/dto/request"
	"time"
)

type ReasonTextTranslation struct {
	Lang string `json:"lang" validate:"required"`
	Text string `json:"text" validate:"required"`
}

type AccrualInput struct {
	UserID                 uint                     `json:"user_id" validate:"required"`
	Amount                 int                      `json:"amount" validate:"required"`
	AccruedAt              string                   `json:"accrued_at" validate:"required,datetime=2006-01-02T15:04:05Z07:00"`
	ReasonID               *string                  `json:"reason_id"`
	ReasonType             string                   `json:"reason_type" validate:"required"`
	ReasonTextTranslations []*ReasonTextTranslation `json:"reason_text_translations" validate:"required,min=1,dive"`
	AppSlug                string                   `json:"app_slug" validate:"required"`
}

func (i *AccrualInput) ToAccrualDTO() (*request.AccrualDTO, error) {
	accruedAt, err := time.Parse(time.RFC3339, i.AccruedAt)
	if err != nil {
		return nil, fmt.Errorf("accrual at time '%s' parse failed: %s", i.AccruedAt, err)
	}

	var reasonID *string
	if i.ReasonID != nil && *i.ReasonID != "" {
		reasonID = i.ReasonID
	}

	rtt := make(map[request.Lang]string)
	for _, t := range i.ReasonTextTranslations {
		rtt[request.Lang(t.Lang)] = t.Text
	}

	return request.NewAccrualDTO(i.UserID, i.Amount, accruedAt, reasonID, i.ReasonType, rtt, i.AppSlug), nil
}
