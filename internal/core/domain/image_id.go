package domain

import "github.com/google/uuid"

type ImageID struct {
	id string
}

func NewImageID() *ImageID {
	return &ImageID{uuid.New().String()}
}

func NewImageIDFromString(id string) *ImageID {
	return &ImageID{id}
}

func (id *ImageID) Value() string {
	return id.id
}
