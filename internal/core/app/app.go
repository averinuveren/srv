package app

import (
	"context"
	"errors"
	"fmt"
	coinRequest "shop-service/internal/coin/app/dto/request"
	"shop-service/internal/draw/app/dto/request"
	sharedApp "shop-service/internal/shared/app"
	"time"

	coinApp "shop-service/internal/coin/app"
	coinDomain "shop-service/internal/coin/domain"
	dtoRequest "shop-service/internal/core/app/dto/request"
	dtoResponse "shop-service/internal/core/app/dto/response"
	"shop-service/internal/core/domain"
	drawApp "shop-service/internal/draw/app"
	drawDomain "shop-service/internal/draw/domain"
	sharedDomain "shop-service/internal/shared/domain"
)

type Application interface {
	All(ctx context.Context) ([]dtoResponse.LotDTO, error)
	Create(ctx context.Context, dto dtoRequest.CreateLotDTO) error
	GetByID(ctx context.Context, id string) (*dtoResponse.LotDTO, error)
	Update(ctx context.Context, dto dtoRequest.UpdateLotDTO) error
	Delete(ctx context.Context, id string) error
	LotsForUserAllByPeriod(ctx context.Context, userID uint) (*dtoResponse.LotForUserByPeriodDTO, error)
	BuyLot(ctx context.Context, dto dtoRequest.BuyLotDTO) error
	ActiveWhereUserBuyTicket(ctx context.Context, userID uint) ([]dtoResponse.LotForUserDTO, error)
}

type applicationImpl struct {
	r         domain.Repository
	txManager sharedApp.TxManager
	drawServ  drawApp.Application
	coinServ  coinApp.Application
}

func ProvideApplicationImpl(
	r domain.Repository,
	txManager sharedApp.TxManager,
	drawServ drawApp.Application,
	coinServ coinApp.Application,
) Application {
	return &applicationImpl{
		r:         r,
		txManager: txManager,
		drawServ:  drawServ,
		coinServ:  coinServ,
	}
}

func (a *applicationImpl) All(ctx context.Context) ([]dtoResponse.LotDTO, error) {
	lots, err := a.r.GetAll(ctx)
	if err != nil {
		return nil, err
	}
	lotsDTO := make([]dtoResponse.LotDTO, len(lots))
	for i, l := range lots {
		switch l.ProductType() {
		case domain.ProductTypeDraw:
			d, err := a.drawServ.GetByLotID(ctx, *l.ID())
			if err != nil {
				return nil, err
			}
			l.SetProduct(d)
		}
		lotsDTO[i] = *dtoResponse.EntityToLotDTO(*l)
	}
	return lotsDTO, nil
}

func (a *applicationImpl) Create(ctx context.Context, dto dtoRequest.CreateLotDTO) error {
	l, err := dtoRequest.CreateLotDTOToEntity(dto)
	if err != nil {
		return fmt.Errorf("create lot dto to entity failed: %s", err.Error())
	}
	if err := a.r.Store(ctx, *l); err != nil {
		return fmt.Errorf("log store failed: %s", err.Error())
	}

	pt, err := domain.NewProductTypeFromString(dto.ProductType)
	if err != nil {
		return fmt.Errorf("new product type failed: %s", err)
	}
	switch *pt {
	case domain.ProductTypeDraw:
		if dto.Draw == nil {
			return fmt.Errorf("draw must be not nil with product type %s", pt.String())
		}
		if err := a.drawServ.Create(ctx, *drawApp.NewCreateDrawDTO(*l.ID(), dto.Draw.WinningPlaces)); err != nil {
			return fmt.Errorf("creaet draw failed: %s", err.Error())
		}
	default:
		return fmt.Errorf("unknown product type")
	}

	return nil
}

func (a *applicationImpl) GetByID(ctx context.Context, id string) (*dtoResponse.LotDTO, error) {
	lotID := domain.NewLotIDFromString(id)
	l, err := a.r.FirstWereID(ctx, *lotID)
	if err != nil {
		return nil, err
	}
	switch l.ProductType() {
	case domain.ProductTypeDraw:
		d, err := a.drawServ.GetByLotID(ctx, *lotID)
		if err != nil {
			return nil, err
		}
		l.SetProduct(d)
	}
	return dtoResponse.EntityToLotDTO(*l), nil
}

func (a *applicationImpl) Update(ctx context.Context, dto dtoRequest.UpdateLotDTO) error {
	lotID := domain.NewLotIDFromString(dto.Id)
	l, err := a.r.FirstWereID(ctx, *lotID)
	if err != nil {
		return err
	}
	l.SetPrice(dto.Price)
	l.SetCount(dto.Count)
	l.SetActiveAt(dto.ActiveAt)
	l.SetInactiveAt(dto.InactiveAt)
	l.SetLimitPerUser(dto.LimitPerUser)

	transl := make(map[sharedDomain.Lang]map[sharedDomain.TranslationField]*string)
	for langDTO, translDTO := range dto.Translations {
		transl[sharedDomain.Lang(langDTO)] = map[sharedDomain.TranslationField]*string{
			domain.Name:        &translDTO.Name,
			domain.Description: &translDTO.Description,
		}
	}
	l.SetTranslations(transl)

	images := make([]*domain.Image, 0)
	for _, imageDTO := range dto.Images {
		if imageDTO.Id == nil && imageDTO.Path != nil {
			images = append(images, domain.NewImage(*domain.NewImageID(), *lotID, *imageDTO.Path))
		}

		if imageDTO.Id != nil {
			for _, i := range l.Images() {
				if i.ID().Value() == *imageDTO.Id {
					if imageDTO.Path != nil {
						i.SetImage(*imageDTO.Path)
					}
					images = append(images, i)
					break
				}
			}
		}
	}
	l.SetImages(images)

	switch l.ProductType() {
	case domain.ProductTypeDraw:
		if dto.Draw == nil {
			return fmt.Errorf("draw required for draw product type")
		}
		if err := a.drawServ.Update(ctx, *drawApp.NewUpdateDrawDTO(*l.ID(), dto.Draw.WinningPlaces)); err != nil {
			return fmt.Errorf("failed update draw: %s", err.Error())
		}
	}

	return a.r.Update(ctx, *l)
}

func (a *applicationImpl) Delete(ctx context.Context, id string) error {
	lotID := *domain.NewLotIDFromString(id)
	if err := a.drawServ.DeleteWhereLotID(ctx, lotID); err != nil {
		return err
	}
	return a.r.Delete(ctx, lotID)
}

func (a *applicationImpl) LotsForUserAllByPeriod(ctx context.Context, userID uint) (*dtoResponse.LotForUserByPeriodDTO, error) {
	lots, err := a.r.GetAll(ctx)
	if err != nil {
		return nil, fmt.Errorf("get all failed: %s", err)
	}

	var upcoming, current []*dtoResponse.LotForUserDTO

	for _, l := range lots {
		lotForUserDTO := dtoResponse.EntityToLotForUserDTO(*l)

		switch l.ProductType() {
		case domain.ProductTypeDraw:
			lotForUserDTO.Draw = &dtoResponse.DrawInfo{
				PrizesCount:        l.Product().(*drawDomain.Draw).WinningPlaces(),
				BoughtTicketsCount: 0,
			}
		}

		// upcoming
		if l.ActiveAt() == nil || l.ActiveAt().Unix() > time.Now().Unix() {
			upcoming = append(upcoming, lotForUserDTO)
			continue
		}

		// current
		if l.ActiveAt().Unix() < time.Now().Unix() {
			switch l.ProductType() {
			case domain.ProductTypeDraw:
				dto := request.NewGetUserDrawTicketsDTO(userID, l.Product().ProductID())
				tickets, err := a.drawServ.GetUserDrawTickets(ctx, *dto)
				if err != nil {
					return nil, fmt.Errorf("get user draw tickets failed: %s", err.Error())
				}
				lotForUserDTO.Draw.BoughtTicketsCount = uint(len(tickets))
			}

			current = append(current, lotForUserDTO)
			continue
		}
	}

	return dtoResponse.NewLotForUserByPeriodDTO(current, upcoming, nil), nil
}

var (
	LotNotFound             = errors.New("lot not found")
	LotInactive             = errors.New("lot inactive")
	InsufficientCount       = errors.New("insufficient count")
	PriceNotSet             = errors.New("price not set")
	NotEnoughCoinsForBuy    = errors.New("not enough coins for buy")
	LotLimitPerUserExceeded = errors.New("lot limit per user exceeded")
)

func (a *applicationImpl) BuyLot(ctx context.Context, dto dtoRequest.BuyLotDTO) error {
	lotID := domain.NewLotIDFromString(dto.LotID())

	lot, err := a.r.FirstWereID(ctx, *lotID)
	if err != nil {
		if errors.Is(err, domain.NofFound) {
			return LotNotFound
		}
		return fmt.Errorf("first lot where id %s failed: %s", lotID.Value(), err.Error())
	}

	if !lot.IsActive() {
		return LotInactive
	}

	if lot.LeftoverCount() < dto.Count() {
		return InsufficientCount
	}

	if lot.Price() == nil {
		return PriceNotSet
	}

	userCoinAmount, err := a.coinServ.GetUserCoinAmount(ctx, dto.UserID())
	if err != nil {
		return fmt.Errorf("get user coin amount failed: %s", err.Error())
	}
	totalPrice := *lot.Price() * dto.Count()
	if int(totalPrice) > *userCoinAmount {
		return NotEnoughCoinsForBuy
	}

	if lot.LimitPerUser() != nil {
		reasonID := lot.ID().Value()
		prevCoin, err := a.coinServ.GetUserWhereReason(ctx, *coinRequest.NewGetUserWhereReasonDTO(
			dto.UserID(),
			&reasonID,
			coinDomain.BuyLot,
		))
		if err != nil {
			return fmt.Errorf("get prev lot coins failed: %s", err.Error())
		}
		if int(*lot.LimitPerUser()) < (int(dto.Count()) + len(prevCoin)) {
			return LotLimitPerUserExceeded
		}
	}

	tx, err := a.txManager.Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin transaction failed: %s", err.Error())
	}
	defer tx.Rollback(ctx)

	// set and update lot leftover count
	lot.SetLeftoverCount(lot.LeftoverCount() - dto.Count())
	lotRepoTx := a.r.MakeFromTx(tx)
	err = lotRepoTx.Update(ctx, *lot)
	if err != nil {
		return fmt.Errorf("lot update failed: %s", err.Error())
	}

	// write-off coins
	accruals := make([]coinRequest.AccrualDTO, dto.Count())
	for i := 0; i < int(dto.Count()); i++ {
		transl := make(map[coinRequest.Lang]string)
		for lang, v := range lot.GetTranslations() {
			if v, ok := v[("name")]; ok && v != nil {
				transl[coinRequest.Lang(lang)] = *v
			}
		}
		reasonID := lot.ID().Value()
		accrualDTO := coinRequest.NewAccrualDTO(
			dto.UserID(),
			-int(*lot.Price()),
			time.Now(),
			&reasonID,
			coinDomain.BuyLot.String(),
			transl,
			dto.AppSlug(),
		)
		accruals[i] = *accrualDTO
	}
	err = a.coinServ.AccrualMany(ctx, accruals)
	if err != nil {
		// TODO: compensating transaction
		return fmt.Errorf("write-off coins failed: %s", err.Error())
	}

	// run lot product
	switch lot.ProductType() {
	case domain.ProductTypeDraw:
		err = a.drawServ.AddTicketsForUser(ctx, *request.NewAddTicketsForUserDTO(
			dto.UserID(),
			*lot.Product().(*drawDomain.Draw).ID(),
			dto.Count(),
		))
		if err != nil {
			// TODO: compensating transaction
			return fmt.Errorf("add ticket for user failed: %s", err.Error())
		}
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("commit failed: %s", err.Error())
	}

	return nil
}

func (a *applicationImpl) ActiveWhereUserBuyTicket(ctx context.Context, userID uint) ([]dtoResponse.LotForUserDTO, error) {
	lots, err := a.r.GetWhereTimeBetweenActiveAndInactive(ctx, time.Now())
	if err != nil {
		return nil, fmt.Errorf("get active lots failed: %s", err.Error())
	}

	var lotsForUsersDTO []dtoResponse.LotForUserDTO

	for _, l := range lots {
		d := request.NewGetUserDrawTicketsDTO(userID, l.Product().ProductID())
		tickets, err := a.drawServ.GetUserDrawTickets(ctx, *d)
		if err != nil {
			return nil, fmt.Errorf("get user draw tickets failed: %s", err)
		}
		if tickets != nil {
			lotForUserDTO := dtoResponse.EntityToLotForUserDTO(*l)
			lotForUserDTO.Draw = &dtoResponse.DrawInfo{
				PrizesCount:        l.Product().(*drawDomain.Draw).WinningPlaces(),
				BoughtTicketsCount: uint(len(tickets)),
			}
			lotsForUsersDTO = append(lotsForUsersDTO, *lotForUserDTO)
		}
	}

	return lotsForUsersDTO, nil
}
