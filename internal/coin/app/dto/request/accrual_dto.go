package request

import (
	"time"
)

type Lang string

type AccrualDTO struct {
	userID                uint
	amount                int
	accruedAt             time.Time
	reasonID              *string
	reasonType            string
	reasonTextTransaction map[Lang]string
	appSlug               string
}

func NewAccrualDTO(
	userID uint,
	amount int,
	accruedAt time.Time,
	reasonID *string,
	reasonType string,
	reasonTextTransaction map[Lang]string,
	appSlug string,
) *AccrualDTO {
	return &AccrualDTO{
		userID:                userID,
		amount:                amount,
		accruedAt:             accruedAt,
		reasonID:              reasonID,
		reasonType:            reasonType,
		reasonTextTransaction: reasonTextTransaction,
		appSlug:               appSlug,
	}
}
