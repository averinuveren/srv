package request

import (
	"fmt"
	"shop-service/internal/core/domain"
	sharedDomain "shop-service/internal/shared/domain"
)

func CreateLotDTOToEntity(dto CreateLotDTO) (*domain.Lot, error) {
	lotID := *domain.NewLotID()

	images := make([]*domain.Image, len(dto.Images))
	for index, i := range dto.Images {
		images[index] = domain.NewImage(*domain.NewImageID(), lotID, i)
	}

	transl := make(map[sharedDomain.Lang]map[sharedDomain.TranslationField]*string)
	for l, t := range dto.Translations {
		transl[sharedDomain.Lang(l)] = map[sharedDomain.TranslationField]*string{
			domain.Name:        &t.Name,
			domain.Description: &t.Description,
		}
	}

	pt, err := domain.NewProductTypeFromString(dto.ProductType)
	if err != nil {
		return nil, fmt.Errorf("new product type failed: %s", err)
	}

	return domain.NewLot(
		lotID,
		"",
		"",
		dto.Price,
		dto.Count,
		dto.Count,
		dto.ActiveAt,
		dto.InactiveAt,
		dto.LimitPerUser,
		images,
		*pt,
		nil,
		transl,
		"ru",
	), nil
}
