create table if not exists lot_images
(
    id         uuid primary key,
    lot_id     uuid         not null,
    image      varchar(255) not null,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_lot_images_lot_id
        FOREIGN KEY (lot_id)
            REFERENCES lots (id)
);
