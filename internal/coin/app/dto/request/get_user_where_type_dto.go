package request

import "shop-service/internal/coin/domain"

type GetUserWhereReasonDTO struct {
	userID     uint
	reasonID   *string
	reasonType domain.ReasonType
}

func NewGetUserWhereReasonDTO(
	userID uint,
	reasonID *string,
	reasonType domain.ReasonType,
) *GetUserWhereReasonDTO {
	return &GetUserWhereReasonDTO{
		userID:     userID,
		reasonID:   reasonID,
		reasonType: reasonType,
	}
}

func (g *GetUserWhereReasonDTO) UserID() uint {
	return g.userID
}

func (g *GetUserWhereReasonDTO) ReasonID() *string {
	return g.reasonID
}

func (g *GetUserWhereReasonDTO) ReasonType() domain.ReasonType {
	return g.reasonType
}
