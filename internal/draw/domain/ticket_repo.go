package domain

import (
	"context"
	"shop-service/internal/shared/infra/transactionmanager"
)

type TicketRepository interface {
	Store(ctx context.Context, ticket Ticket) error
	GetWhereUserID(ctx context.Context, userID uint) ([]*Ticket, error)
	GetWhereUserIDAndDrawID(ctx context.Context, userID uint, drawID DrawID) ([]*Ticket, error)
	MakeFromTx(tx transactionmanager.Tx) TicketRepository
}
