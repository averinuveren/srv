create table if not exists cms_users
(
    id         serial primary key,
    name       varchar(50)        not null default '',
    email      varchar(50) unique not null,
    password   varchar(255)       not null,
    avatar     varchar(255)       not null default '',
    created_at timestamp,
    updated_at timestamp
);

insert into cms_users (name, email, password, created_at, updated_at)
values ('admin', 'admin@mail.ru', '$2a$04$kyyLS/JxXh3biww2vC6sqeuj/7VzHhavXuJvKbqp.Lm31LDu2rTW2', now(), now());
