package response

type Image struct {
	ID  string `json:"id"`
	Url string `json:"url"`
}

func NewImage(ID string, url string) *Image {
	return &Image{ID: ID, Url: url}
}

type Draw struct {
	WinningPlaces uint `json:"winning_places"`
}

func NewDraw(winningPlaces uint) *Draw {
	return &Draw{WinningPlaces: winningPlaces}
}
