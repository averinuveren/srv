package request

import (
	"time"
)

type CreateLotDTO struct {
	Price        *uint
	Count        uint
	ActiveAt     *time.Time
	InactiveAt   *time.Time
	LimitPerUser *uint
	Images       []string
	ProductType  string
	Draw         *Draw
	Translations map[Lang]*LotTransl
}

func NewCreateLotDTO(
	price *uint,
	count uint,
	activeAt *time.Time,
	inactiveAt *time.Time,
	limitPerUser *uint,
	images []string,
	productType string,
	draw *Draw,
	translations map[Lang]*LotTransl,
) (*CreateLotDTO, error) {
	return &CreateLotDTO{
		Price:        price,
		Count:        count,
		ActiveAt:     activeAt,
		InactiveAt:   inactiveAt,
		LimitPerUser: limitPerUser,
		Images:       images,
		ProductType:  productType,
		Draw:         draw,
		Translations: translations,
	}, nil
}
