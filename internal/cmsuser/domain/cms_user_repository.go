package domain

type CmsUserRepository interface {
	GetByID(id uint) (*CmsUser, error)
	GetByEmail(email string) (*CmsUser, error)
	Update(cu CmsUser) error
	Store(name string, email string, password string) error
}
