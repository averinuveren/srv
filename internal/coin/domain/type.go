package domain

import (
	"fmt"
)

var types = []string{
	"scratch_workout_completion",
	"reward_profile_completion",
	"fill_profile",
	"scratch_test_exam",
	"water_profit",
	"workout_result",
	"step_profit",
	"buy_lot",
	"workout_completion",
	"onboarding_completion_buttocks",
}

type ReasonType uint

func NewTypeFromString(s string) (*ReasonType, error) {
	for i, v := range types {
		if v == s {
			t := ReasonType(i)
			return &t, nil
		}
	}
	return nil, fmt.Errorf("couldn't create type: type '%s' does not exist", s)
}

func (t ReasonType) String() string {
	return types[t]
}

const (
	ScratchWorkoutCompletion ReasonType = iota
	RewardProfileCompletion
	FillProfile
	ScratchTestExam
	WaterProfit
	WorkoutResult
	StepProfit
	BuyLot
	WorkoutCompletion
)
