package inputs

import (
	"mime/multipart"
	dtoRequest "shop-service/internal/core/app/dto/request"
	"time"
)

type Image struct {
	Image     *multipart.FileHeader `mapstructure:"image" validate:"required,file"`
	ImagePath *string
}

type CreateLotInput struct {
	Price        *uint                       `mapstructure:"price"`
	Count        uint                        `mapstructure:"count"`
	ActiveAt     *string                     `mapstructure:"active_at" validate:"omitempty,datetime=2006-01-02 15:04"`
	InactiveAt   *string                     `mapstructure:"inactive_at" validate:"omitempty,datetime=2006-01-02 15:04"`
	LimitPerUser *uint                       `mapstructure:"limit_per_user"`
	Images       map[uint]*Image             `mapstructure:"images" validate:"required,dive"`
	ProductType  string                      `mapstructure:"product_type" validate:"required"`
	Draw         *Draw                       `mapstructure:"draw" validate:"required_if=ProductType draw,dive"`
	Translations map[string]*LotTranslations `mapstructure:"translations" validate:"required,min=1,dive,lot-translations"`
}

func (i *CreateLotInput) ToCreateLotDTO() (*dtoRequest.CreateLotDTO, error) {
	var err error

	var activeAt *time.Time
	if i.ActiveAt != nil {
		activeAt = new(time.Time)
		*activeAt, err = time.Parse("2006-01-02 15:04", *i.ActiveAt)
		if err != nil {
			return nil, err
		}
	}

	var inactiveAt *time.Time
	if i.InactiveAt != nil {
		inactiveAt = new(time.Time)
		*inactiveAt, err = time.Parse("2006-01-02 15:04", *i.InactiveAt)
		if err != nil {
			return nil, err
		}
	}

	images := make([]string, len(i.Images))
	for index, v := range i.Images {
		images[index] = *v.ImagePath
	}

	transl := make(map[dtoRequest.Lang]*dtoRequest.LotTransl)
	for lang, t := range i.Translations {
		transl[dtoRequest.Lang(lang)] = dtoRequest.NewLotTransl(t.Name, t.Description)
	}

	var draw *dtoRequest.Draw
	if i.Draw != nil {
		draw = dtoRequest.NewDraw(i.Draw.WinningPlaces)
	}

	return dtoRequest.NewCreateLotDTO(
		i.Price,
		i.Count,
		activeAt,
		inactiveAt,
		i.LimitPerUser,
		images,
		i.ProductType,
		draw,
		transl,
	)
}
