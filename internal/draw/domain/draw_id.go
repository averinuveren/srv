package domain

import "github.com/google/uuid"

type DrawID struct {
	id string
}

func NewDrawID() *DrawID {
	return &DrawID{uuid.New().String()}
}

func (did *DrawID) Value() string {
	return did.id
}

func NewDrawIDFromString(id string) *DrawID {
	return &DrawID{id}
}
