package domain

import "errors"

var (
	NotFound = errors.New("cms user not found")
)
