package dto

type LoginDTO struct {
	email    string
	password string
}

func NewLoginDTO(email string, password string) *LoginDTO {
	return &LoginDTO{email: email, password: password}
}

func (l *LoginDTO) Email() string {
	return l.email
}

func (l *LoginDTO) Password() string {
	return l.password
}
