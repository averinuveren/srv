package domain

import (
	"context"
	lotDomain "shop-service/internal/core/domain"
	"shop-service/internal/shared/infra/transactionmanager"
)

type Repository interface {
	Store(ctx context.Context, draw Draw) error
	FirstWereLotID(ctx context.Context, lotID lotDomain.LotID) (*Draw, error)
	Update(ctx context.Context, draw Draw) error
	Delete(ctx context.Context, drawID DrawID) error
	MakeFromTx(tx transactionmanager.Tx) Repository
}
