package request

import "shop-service/internal/draw/domain"

type AddTicketsForUserDTO struct {
	userID uint
	drawID domain.DrawID
	count  uint
}

func NewAddTicketsForUserDTO(
	userID uint,
	drawID domain.DrawID,
	count uint,
) *AddTicketsForUserDTO {
	return &AddTicketsForUserDTO{
		userID: userID,
		drawID: drawID,
		count:  count,
	}
}

func (a *AddTicketsForUserDTO) UserID() uint {
	return a.userID
}

func (a *AddTicketsForUserDTO) DrawID() domain.DrawID {
	return a.drawID
}

func (a *AddTicketsForUserDTO) Count() uint {
	return a.count
}
