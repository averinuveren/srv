package domain

import (
	"fmt"
)

type (
	Lang             string
	TranslationField string
)

type MultilangEntity interface {
	GetCurrLang() Lang
	SetLang(lang Lang)
	GetTranslations() map[Lang]map[TranslationField]*string
	SetTranslations(map[Lang]map[TranslationField]*string)
	AddTranslation(lang Lang, d map[TranslationField]*string) error
	GetTranslationFields() []TranslationField
}

type AbstractMultilangEntity struct {
	MultilangEntity
	currentLang  Lang
	translations map[Lang]map[TranslationField]*string
}

func NewAbstractMultilangEntity(
	currentLang Lang,
	translations map[Lang]map[TranslationField]*string,
) *AbstractMultilangEntity {
	return &AbstractMultilangEntity{
		currentLang:  currentLang,
		translations: translations,
	}
}

func (a *AbstractMultilangEntity) GetCurrLang() Lang {
	return a.currentLang
}

func (a *AbstractMultilangEntity) SetLang(lang Lang) {
	a.currentLang = lang
}

func (a *AbstractMultilangEntity) GetTranslations() map[Lang]map[TranslationField]*string {
	return a.translations
}

func (a *AbstractMultilangEntity) SetTranslations(trans map[Lang]map[TranslationField]*string) {
	a.translations = trans
}

func (a *AbstractMultilangEntity) AddTranslation(lang Lang, d map[TranslationField]*string) error {
	for _, f := range a.GetTranslationFields() {
		if _, ok := d[f]; !ok {
			return fmt.Errorf("transition fild %s does not exist in map", f)
		}
	}
	a.translations[lang] = d
	return nil
}
