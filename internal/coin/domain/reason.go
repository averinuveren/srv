package domain

import sharedDomain "shop-service/internal/shared/domain"

type Reason struct {
	reasonID         *string
	reasonType       ReasonType
	textTranslations map[sharedDomain.Lang]string
	appSlug          string
}

func NewReason(
	reasonID *string,
	reasonType ReasonType,
	textTranslations map[sharedDomain.Lang]string,
	appSlug string,
) *Reason {
	return &Reason{
		reasonID:         reasonID,
		reasonType:       reasonType,
		textTranslations: textTranslations,
		appSlug:          appSlug,
	}
}

func (r *Reason) ReasonID() *string {
	return r.reasonID
}

func (r *Reason) ReasonType() ReasonType {
	return r.reasonType
}

func (r *Reason) TextTranslations() map[sharedDomain.Lang]string {
	return r.textTranslations
}

func (r *Reason) AppSlug() string {
	return r.appSlug
}
