package domain

import "errors"

var (
	NofFound = errors.New("lot not found")
)
