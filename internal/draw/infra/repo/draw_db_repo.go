package repo

import (
	"context"
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	lotDomain "shop-service/internal/core/domain"
	"shop-service/internal/draw/domain"
	"shop-service/internal/shared/app"
)

const drawTableName = "draws"

type DrawDBRepo struct {
	db pgxtype.Querier
}

func ProvideDrawDBRepo(db *pgxpool.Pool) domain.Repository {
	return &DrawDBRepo{db: db}
}

func (r *DrawDBRepo) Store(ctx context.Context, d domain.Draw) error {
	sql := fmt.Sprintf(`insert into %s (id, lot_id, winning_places, created_at, updated_at)
		values ($1, $2, $3, now(), now())`, drawTableName)
	if _, err := r.db.Exec(ctx, sql, d.ID().Value(), d.LotID().Value(), d.WinningPlaces()); err != nil {
		return fmt.Errorf("exec failed: %s", err.Error())
	}
	return nil
}

func (r *DrawDBRepo) FirstWereLotID(ctx context.Context, lotID lotDomain.LotID) (*domain.Draw, error) {
	sql := fmt.Sprintf(`select id, winning_places from %s where lot_id=$1`, drawTableName)
	row := r.db.QueryRow(ctx, sql, lotID.Value())
	var (
		id            string
		winningPlaces uint
	)
	if err := row.Scan(&id, &winningPlaces); err != nil {
		return nil, fmt.Errorf("draw scan failed: %s", err.Error())
	}
	return domain.NewDraw(*domain.NewDrawIDFromString(id), lotID, winningPlaces, nil), nil
}

func (r *DrawDBRepo) Update(ctx context.Context, draw domain.Draw) error {
	sql := fmt.Sprintf("update %s set winning_places=$1 where id=$2", drawTableName)
	_, err := r.db.Exec(ctx, sql, draw.WinningPlaces(), draw.ID().Value())
	if err != nil {
		return fmt.Errorf("update exec failed: %s", err)
	}
	return nil
}

func (r *DrawDBRepo) Delete(ctx context.Context, drawID domain.DrawID) error {
	sql := fmt.Sprintf(`delete from %s where id =$1`, drawTableName)
	if _, err := r.db.Exec(ctx, sql, drawID.Value()); err != nil {
		return fmt.Errorf("exec failed: %s", err.Error())
	}
	return nil
}

func (r DrawDBRepo) MakeFromTx(tx app.Tx) domain.Repository {
	r.db = tx.Conn().(pgx.Tx)
	return &r
}
