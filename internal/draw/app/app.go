package app

import (
	"context"
	"fmt"
	sharedApp "shop-service/internal/shared/app"

	lotDomain "shop-service/internal/core/domain"
	"shop-service/internal/draw/app/dto/request"
	"shop-service/internal/draw/domain"
)

type CreateDrawDTO struct {
	lotID         lotDomain.LotID
	winningPlaces uint
}

func NewCreateDrawDTO(lotID lotDomain.LotID, winningPlaces uint) *CreateDrawDTO {
	return &CreateDrawDTO{lotID: lotID, winningPlaces: winningPlaces}
}

type UpdateDrawDTO struct {
	lotID         lotDomain.LotID
	winningPlaces uint
}

func NewUpdateDrawDTO(lotID lotDomain.LotID, winningPlaces uint) *UpdateDrawDTO {
	return &UpdateDrawDTO{lotID: lotID, winningPlaces: winningPlaces}
}

type Application interface {
	Create(ctx context.Context, dto CreateDrawDTO) error
	GetByLotID(ctx context.Context, lotID lotDomain.LotID) (*domain.Draw, error) // TODO: response dto
	Update(ctx context.Context, dto UpdateDrawDTO) error
	DeleteWhereLotID(ctx context.Context, lotID lotDomain.LotID) error
	AddTicketsForUser(ctx context.Context, dto request.AddTicketsForUserDTO) error
	GetUserTickets(ctx context.Context, userID uint) ([]*domain.Ticket, error)                           // TODO: response dto
	GetUserDrawTickets(ctx context.Context, dto request.GetUserDrawTicketsDTO) ([]*domain.Ticket, error) // TODO: response dto
}

type applicationImpl struct {
	r          domain.Repository
	ticketRepo domain.TicketRepository
	txManager  sharedApp.TxManager
}

func ProvideApplicationImpl(
	r domain.Repository,
	ticketRepo domain.TicketRepository,
	txManager sharedApp.TxManager,
) Application {
	return &applicationImpl{
		r:          r,
		ticketRepo: ticketRepo,
		txManager:  txManager,
	}
}

func (a *applicationImpl) Create(ctx context.Context, dto CreateDrawDTO) error {
	return a.r.Store(ctx, *domain.NewDraw(*domain.NewDrawID(), dto.lotID, dto.winningPlaces, nil))
}

func (a *applicationImpl) GetByLotID(ctx context.Context, lotID lotDomain.LotID) (*domain.Draw, error) {
	return a.r.FirstWereLotID(ctx, lotID)
}

func (a *applicationImpl) Update(ctx context.Context, dto UpdateDrawDTO) error {
	draw, err := a.r.FirstWereLotID(ctx, dto.lotID)
	if err != nil {
		return fmt.Errorf("draw with lotID %s not found", dto.lotID.Value())
	}
	draw.SetWinningPlaces(dto.winningPlaces)
	return a.r.Update(ctx, *draw)
}

func (a *applicationImpl) DeleteWhereLotID(ctx context.Context, lotID lotDomain.LotID) error {
	d, err := a.r.FirstWereLotID(ctx, lotID)
	if err != nil {
		return fmt.Errorf("lot %s not found: %s", lotID.Value(), err.Error())
	}
	return a.r.Delete(ctx, *d.ID())
}

func (a *applicationImpl) AddTicketsForUser(ctx context.Context, dto request.AddTicketsForUserDTO) error {
	tx, err := a.txManager.Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin transaction failed: %s", err.Error())
	}
	defer tx.Rollback(ctx)

	ticketRxRepo := a.ticketRepo.MakeFromTx(tx)

	for i := 0; i < int(dto.Count()); i++ {
		ticket := domain.MakeTicketWithHash(dto.DrawID(), dto.UserID())
		err := ticketRxRepo.Store(ctx, *ticket)
		if err != nil {
			return fmt.Errorf("store ticket failed: %s", err.Error())
		}
	}

	return nil
}

func (a *applicationImpl) GetUserTickets(ctx context.Context, userID uint) ([]*domain.Ticket, error) {
	return a.ticketRepo.GetWhereUserID(ctx, userID)
}

func (a *applicationImpl) GetUserDrawTickets(ctx context.Context, dto request.GetUserDrawTicketsDTO) ([]*domain.Ticket, error) {
	drawID := domain.NewDrawIDFromString(dto.DrawID())
	return a.ticketRepo.GetWhereUserIDAndDrawID(ctx, dto.UserID(), *drawID)
}
