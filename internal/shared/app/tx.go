package app

import "context"

type Tx interface {
	Commit(ctx context.Context) error
	Rollback(ctx context.Context)
	Conn() interface{}
}

type TxManager interface {
	Begin(ctx context.Context) (Tx, error)
}
