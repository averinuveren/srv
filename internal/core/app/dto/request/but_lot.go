package request

type BuyLotDTO struct {
	userID  uint
	lotID   string
	count   uint
	appSlug string
}

func (b *BuyLotDTO) UserID() uint {
	return b.userID
}

func (b *BuyLotDTO) LotID() string {
	return b.lotID
}

func (b *BuyLotDTO) Count() uint {
	return b.count
}

func (b *BuyLotDTO) AppSlug() string {
	return b.appSlug
}

func NewBuyLotDTO(userID uint, lotID string, count uint, appSlug string) *BuyLotDTO {
	return &BuyLotDTO{userID: userID, lotID: lotID, count: count, appSlug: appSlug}
}
