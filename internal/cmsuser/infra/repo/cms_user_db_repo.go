package repo

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"shop-service/internal/cmsuser/domain"
)

type cmsUserDBRepo struct {
	db *pgxpool.Pool
}

func ProvideCmsUserDBRepo(db *pgxpool.Pool) domain.CmsUserRepository {
	return &cmsUserDBRepo{db: db}
}

func (r *cmsUserDBRepo) GetByID(id uint) (*domain.CmsUser, error) {
	sql := "select * from cms_users where id = $1"
	row := r.db.QueryRow(context.Background(), sql, id)
	cu, err := scanRow(row)
	if err != nil {
		return nil, err
	}
	return cu, nil
}

func (r *cmsUserDBRepo) GetByEmail(email string) (*domain.CmsUser, error) {
	sql := "select * from cms_users where email = $1"
	row := r.db.QueryRow(context.Background(), sql, email)
	cu, err := scanRow(row)
	if err != nil {
		return nil, err
	}
	return cu, nil
}

func (r *cmsUserDBRepo) Store(name string, email string, password string) error {
	sql := `insert into cms_users (name, email, password, created_at, updated_at)
			values ($1, $2, $3, now(), now());`
	if _, err := r.db.Exec(context.Background(), sql, name, email, password); err != nil {
		return err
	}
	return nil
}

func (r *cmsUserDBRepo) Update(cu domain.CmsUser) error {
	sql := `update cms_users
			set name = $1,
				email = $2,
				password = $3,
				avatar = $4,
				updated_at = now()
				where id = $5;`
	_, err := r.db.Exec(context.Background(), sql, cu.Name(), cu.Email(), cu.Password(), cu.Avatar(), cu.Id())
	if err != nil {
		return err
	}
	return nil
}

func scanRow(r pgx.Row) (*domain.CmsUser, error) {
	var (
		id       uint
		name     string
		email    string
		password string
		avatar   string
	)
	switch err := r.Scan(&id, &name, &email, &password, &avatar, nil, nil); err {
	case pgx.ErrNoRows:
		return nil, domain.NotFound
	case nil:
		u := domain.NewCmsUser(id, name, email, password, avatar)
		return u, nil
	default:
		return nil, err
	}
}
