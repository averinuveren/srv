package http

import (
	"github.com/gorilla/mux"
	"net/http"
	cuControllers "shop-service/internal/cmsuser/ui/http/controllers"
	lotControllers "shop-service/internal/core/ui/http/controllers"
	openApiControllers "shop-service/internal/shared/ui/http/controllers"
	"shop-service/internal/shared/ui/http/middlewares"
)

func NewRouter(
	oac *openApiControllers.OpenAPIController,
	cuc *cuControllers.CmsUserController,
	lc *lotControllers.LotController,
) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	apiRouter := router.PathPrefix("/api/").Subrouter().StrictSlash(true)
	apiRouter.Use(middlewares.ApiHeaders, middlewares.Cors, middlewares.Language)

	// health check
	router.HandleFunc("/", func(rw http.ResponseWriter, _ *http.Request) {
		rw.WriteHeader(http.StatusOK)
		_, _ = rw.Write([]byte("OK"))
	}).Methods(http.MethodGet, http.MethodOptions)

	// openapi
	router.HandleFunc("/openapi", oac.Index).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/openapi.yaml", oac.Yaml).Methods(http.MethodGet, http.MethodOptions)

	// cms user
	apiRouter.HandleFunc("/cms_user/login", cuc.Login).Methods(http.MethodPost, http.MethodOptions)
	apiRouter.HandleFunc("/cms_user", middlewares.Auth(cuc.Create)).Methods(http.MethodPost, http.MethodOptions)
	apiRouter.HandleFunc("/cms_user", middlewares.Auth(cuc.Update)).Methods(http.MethodPut, http.MethodOptions)

	// lot
	apiRouter.HandleFunc("/lot", middlewares.Auth(lc.Index)).Methods(http.MethodGet, http.MethodOptions)
	apiRouter.HandleFunc("/lot/{id}", middlewares.Auth(lc.GetByID)).Methods(http.MethodGet, http.MethodOptions)
	apiRouter.HandleFunc("/lot", middlewares.Auth(lc.Create)).Methods(http.MethodPost, http.MethodOptions)
	apiRouter.HandleFunc("/lot/{id}", middlewares.Auth(lc.Update)).Methods(http.MethodPut, http.MethodOptions)
	apiRouter.HandleFunc("/lot/{id}", middlewares.Auth(lc.Delete)).Methods(http.MethodDelete, http.MethodOptions)

	return router
}
