package domain

import "errors"

var (
	NotFound = errors.New("coin not found")
)
