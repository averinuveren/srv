package ctxkey

import "context"

type ContextKey string

const (
	ContextKeyCmsUserID = ContextKey("cms_user_id")
	ContextKeyLang      = ContextKey("lang")
)

func GetCmsUserID(ctx context.Context) (uint, bool) {
	id, ok := ctx.Value(ContextKeyCmsUserID).(uint)
	return id, ok
}

func GetLang(ctx context.Context) string {
	lang, ok := ctx.Value(ContextKeyLang).(string)
	if !ok {
		return "ru"
	}
	return lang
}
