package input

import (
	dtoRequest "shop-service/internal/core/app/dto/request"
)

type BuyLot struct {
	UserID  uint   `json:"user_id" validate:"required"`
	LotID   string `json:"lot_id" validate:"required"`
	Count   uint   `json:"count" validate:"required"`
	AppSlug string `json:"app_slug" validate:"required"`
}

func (i *BuyLot) ToBuyLotDTO() *dtoRequest.BuyLotDTO {
	return dtoRequest.NewBuyLotDTO(
		i.UserID,
		i.LotID,
		i.Count,
		i.AppSlug,
	)
}
