package domain

import "github.com/google/uuid"

type CoinID struct {
	id string
}

func NewCoinID() *CoinID {
	return &CoinID{id: uuid.New().String()}
}

func NewCoinIDFromString(id string) *CoinID {
	return &CoinID{id: id}
}

func (c *CoinID) Value() string {
	return c.id
}
