package domain

import (
	"context"
	"shop-service/internal/shared/infra/transactionmanager"
	"time"
)

type Repository interface {
	GetAll(ctx context.Context) ([]*Lot, error)
	Store(ctx context.Context, lot Lot) error
	FirstWereID(ctx context.Context, lotID LotID) (*Lot, error)
	Update(ctx context.Context, lot Lot) error
	Delete(ctx context.Context, lotID LotID) error
	GetWhereTimeBetweenActiveAndInactive(ctx context.Context, time time.Time) ([]*Lot, error)
	MakeFromTx(tx transactionmanager.Tx) Repository
}
