create table if not exists draws
(
    id             uuid primary key,
    lot_id         uuid not null,
    winning_places int  not null,
    created_at     timestamp,
    updated_at     timestamp,
    CONSTRAINT fk_draws_lot_id
        FOREIGN KEY (lot_id)
            REFERENCES lots (id)
);
