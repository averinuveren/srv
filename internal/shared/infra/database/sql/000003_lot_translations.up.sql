create table if not exists lot_translations
(
    id          serial primary key,
    lot_id      uuid         not null,
    locale      varchar(10)  not null,
    name        varchar(255) not null,
    description varchar(255) not null,
    created_at  timestamp,
    updated_at  timestamp,
    CONSTRAINT fk_lot_translations_lot_id
        FOREIGN KEY (lot_id)
            REFERENCES lots (id)
);
