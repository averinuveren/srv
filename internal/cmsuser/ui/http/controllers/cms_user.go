package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"shop-service/internal/cmsuser/application"
	"shop-service/internal/cmsuser/application/dto"
	"shop-service/internal/cmsuser/domain"
	"shop-service/internal/shared/infra/cloudstorage"
	"shop-service/internal/shared/ui/http/controllers"
	"strings"
	"time"
)

type loginInput struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type createInput struct {
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=6"`
}

type updateInput struct {
	Name     *string               `json:"name"`
	Avatar   *multipart.FileHeader `json:"avatar" validate:"omitempty,file"`
	Email    *string               `json:"email" validate:"omitempty,email"`
	Password *string               `json:"password" validate:"omitempty,min=6"`
}

type CmsUserController struct {
	controllers.ApiController
	serv application.CmsUserService
}

func ProvideCmsUserController(ac *controllers.ApiController, serv application.CmsUserService) *CmsUserController {
	return &CmsUserController{ApiController: *ac, serv: serv}
}

func (c *CmsUserController) Login(rw http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var i loginInput
	if err := c.MapRequest(rw, r, &i); err != nil {
		return
	}

	d := dto.NewLoginDTO(i.Email, i.Password)
	cu, err := c.serv.Login(*d)
	if err != nil {
		c.UnauthorizedResponse(rw)
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": cu.Id(),
		"iat": time.Now().Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		log.Fatalf("JWT error: %v", err)
	}

	b, _ := json.Marshal(map[string]interface{}{
		"token": tokenString,
		"user":  cmsUserToResp(*cu),
	})
	_, _ = rw.Write(b)
}

func (c *CmsUserController) Create(rw http.ResponseWriter, r *http.Request) {
	var i createInput
	if err := c.MapRequest(rw, r, &i); err != nil {
		return
	}
	d := dto.NewCreateDTO(i.Name, i.Email, i.Password)
	if err := c.serv.Create(*d); err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	rw.WriteHeader(http.StatusCreated)
}

func (c *CmsUserController) Update(rw http.ResponseWriter, r *http.Request) {
	var i updateInput
	if err := c.MapRequest(rw, r, &i); err != nil {
		return
	}
	var avatarPath *string

	if i.Avatar != nil {
		f, err := i.Avatar.Open()
		if err != nil {
			c.InternalErrorResponse(rw, err)
			return
		}
		defer f.Close()
		var buf bytes.Buffer
		if _, err := io.Copy(&buf, f); err != nil {
			c.InternalErrorResponse(rw, err)
			return
		}
		sfn := strings.Split(i.Avatar.Filename, ".")
		avatarPath = new(string)
		*avatarPath = fmt.Sprintf("shop_service/cms_user/%d.%s", time.Now().UnixNano(), sfn[len(sfn)-1])
		if err = cloudstorage.Instance().Upload(*avatarPath, &buf); err != nil {
			c.InternalErrorResponse(rw, err)
			return
		}
	}

	cuID := c.GetCmsUserID(r)
	d := dto.NewUpdateDTO(cuID, i.Name, avatarPath, i.Email, i.Password)
	cu, err := c.serv.Update(*d)
	if err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	b, _ := json.Marshal(cmsUserToResp(*cu))
	_, _ = rw.Write(b)
}

func cmsUserToResp(cu domain.CmsUser) map[string]interface{} {
	var avatar *string
	if len(cu.Avatar()) != 0 {
		path := cu.Avatar()
		avatar = cloudstorage.Instance().Url(&path)
	}
	return map[string]interface{}{
		"id":     cu.Id(),
		"name":   cu.Name(),
		"email":  cu.Email(),
		"avatar": avatar,
	}
}
