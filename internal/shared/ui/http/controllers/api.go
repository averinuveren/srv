package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/imdario/mergo"
	"github.com/mitchellh/mapstructure"
	"io"
	"mime/multipart"
	"net/http"
	"regexp"
	"shop-service/internal/shared/infra/cloudstorage"
	"shop-service/internal/shared/infra/ctxkey"
	"shop-service/internal/shared/infra/logger"
	"strings"
	"time"
)

var (
	incorrectHeader = errors.New("incorrect header")
	invalidJson     = errors.New("invalid json")
)

type ApiController struct {
	Validate *validator.Validate
}

func ProvideApiController(validate *validator.Validate) *ApiController {
	return &ApiController{Validate: validate}
}

func (c *ApiController) GetCmsUserID(r *http.Request) uint {
	id, _ := ctxkey.GetCmsUserID(r.Context())
	return id
}

func (c *ApiController) UploadFile(fh *multipart.FileHeader, dir string) (*string, error) {
	f, err := fh.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, f); err != nil {
		return nil, err
	}
	sfn := strings.Split(fh.Filename, ".")
	path := fmt.Sprintf("%s/%d.%s", dir, time.Now().UnixNano(), sfn[len(sfn)-1])
	if err = cloudstorage.Instance().Upload(path, &buf); err != nil {
		return nil, err
	}
	return &path, nil
}

func (c *ApiController) MapRequest(rw http.ResponseWriter, r *http.Request, i interface{}) error {
	ct := r.Header.Get("Content-Type")
	if ct == "application/json" {
		d := json.NewDecoder(r.Body)
		if err := d.Decode(&i); err != nil {
			err = invalidJson
			c.ErrorResponse(rw, err.Error(), http.StatusBadRequest)
			return err
		}
	} else if strings.Contains(ct, "multipart/form-data") {
		_ = r.ParseMultipartForm(256 * (1 << 20))
		f := multipartFormValuesToMap(r.MultipartForm)
		if err := mapstructure.WeakDecode(f, &i); err != nil {
			c.ErrorResponse(rw, err.Error(), http.StatusInternalServerError)
			return err
		}
	} else {
		err := incorrectHeader
		c.ErrorResponse(rw, err.Error(), http.StatusBadRequest)
		return err
	}

	if err := c.Validate.Struct(i); err != nil {
		logger.Instance().Error(err)
		c.InvalidResponse(rw, err.(validator.ValidationErrors))
		return err
	}
	return nil
}

func (c *ApiController) InvalidResponse(rw http.ResponseWriter, errors validator.ValidationErrors) {
	rw.WriteHeader(http.StatusUnprocessableEntity)
	resp := make([]map[string]string, len(errors))
	for i, err := range errors {
		resp[i] = map[string]string{
			"field":   strings.ToLower(err.Field()),
			"message": fmt.Sprintf("failed on the '%s' tag", err.Tag()),
		}
	}
	b, _ := json.Marshal(resp)
	_, _ = rw.Write(b)
}

func (c *ApiController) UnauthorizedResponse(rw http.ResponseWriter) {
	c.ErrorResponse(rw, "unauthorized", http.StatusUnauthorized)
}

func (c *ApiController) InternalErrorResponse(rw http.ResponseWriter, err error) {
	c.ErrorResponse(rw, err.Error(), http.StatusInternalServerError)
}

func (c *ApiController) ErrorResponse(rw http.ResponseWriter, msg string, statusCode int) {
	logger.Instance().Error(msg)
	rw.WriteHeader(statusCode)
	b, _ := json.Marshal(map[string]string{
		"error": msg,
	})
	_, _ = rw.Write(b)
}

func multipartFormValuesToMap(f *multipart.Form) map[string]interface{} {
	result := make(map[string]interface{})
	re := regexp.MustCompile(`\[([0-9-a-zA-Z_]+)]`)
	for key, v := range f.Value {
		if matches := re.FindAllStringSubmatch(key, -1); len(matches) != 0 {
			_ = mergo.Merge(&result, toMapMPMatches(key, v[0], matches))
		} else {
			result[key] = v[0]
		}
	}
	for key, v := range f.File {
		if matches := re.FindAllStringSubmatch(key, -1); len(matches) != 0 {
			_ = mergo.Merge(&result, toMapMPMatches(key, v[0], matches))
		} else {
			result[key] = v[0]
		}
	}
	return result
}

func toMapMPMatches(key string, v interface{}, m [][]string) map[string]interface{} {
	r := make(map[string]interface{})
	i := strings.Index(key, "[")
	if len(m) != 0 {
		r[key[:i]] = toMapMPMatches(strings.Replace(key[i:], m[0][0], m[0][1], 10), v, m[1:])
	} else {
		r[key] = v
	}
	return r
}
