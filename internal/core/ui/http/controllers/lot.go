package controllers

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"net/http"
	"shop-service/internal/core/app"
	"shop-service/internal/core/ui/http/controllers/inputs"
	"shop-service/internal/shared/ui/http/controllers"
)

type LotController struct {
	controllers.ApiController
	serv app.Application
}

func ProvideLotController(apiController *controllers.ApiController, serv app.Application) *LotController {
	_ = apiController.Validate.RegisterValidation("lot-translations", validateLotTranslations)
	return &LotController{ApiController: *apiController, serv: serv}
}

func (c *LotController) Index(rw http.ResponseWriter, r *http.Request) {
	l, err := c.serv.All(r.Context())
	if err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	b, _ := json.Marshal(l)
	_, _ = rw.Write(b)
}

func (c *LotController) GetByID(rw http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	l, err := c.serv.GetByID(r.Context(), params["id"])
	if err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	b, _ := json.Marshal(l)
	_, _ = rw.Write(b)
}

func (c *LotController) Create(rw http.ResponseWriter, r *http.Request) {
	var i inputs.CreateLotInput
	if err := c.MapRequest(rw, r, &i); err != nil {
		return
	}
	for _, v := range i.Images {
		v.ImagePath, _ = c.UploadFile(v.Image, "shop_service/lot/images")
	}
	dto, err := i.ToCreateLotDTO()
	if err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	if err := c.serv.Create(r.Context(), *dto); err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	rw.WriteHeader(http.StatusCreated)
}

func (c *LotController) Update(rw http.ResponseWriter, r *http.Request) {
	var i inputs.UpdateLotInput
	if err := c.MapRequest(rw, r, &i); err != nil {
		return
	}
	for _, v := range i.Images {
		if v.Image != nil {
			v.ImagePath, _ = c.UploadFile(v.Image, "shop_service/lot/images")
		}
	}
	params := mux.Vars(r)
	i.ID = params["id"]
	dto, err := i.ToUpdateLotDTO()
	if err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	if err := c.serv.Update(r.Context(), *dto); err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	rw.WriteHeader(http.StatusNoContent)
}

func (c *LotController) Delete(rw http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	if err := c.serv.Delete(r.Context(), params["id"]); err != nil {
		c.InternalErrorResponse(rw, err)
		return
	}
	rw.WriteHeader(http.StatusNoContent)
}

func validateLotTranslations(fl validator.FieldLevel) bool {
	trans, ok := fl.Field().Interface().(map[string]*inputs.LotTranslations)
	if !ok {
		return false
	}
	validate := validator.New()
	for _, t := range trans {
		if err := validate.Struct(t); err != nil {
			return false
		}
	}
	return true
}
