package repo

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"shop-service/internal/coin/domain"
	"shop-service/internal/shared/app"
	sharedDomain "shop-service/internal/shared/domain"
	"time"
)

const coinTableName = "coins"

type CoinDBRepo struct {
	db pgxtype.Querier
}

func ProvideCoinDBRepo(db *pgxpool.Pool) domain.Repository {
	return &CoinDBRepo{db: db}
}

func (r *CoinDBRepo) Store(ctx context.Context, coin domain.Coin) error {
	sql := fmt.Sprintf(`insert into %s
		(id, user_id, amount, accrued_at, reason_id, reason_type, reason_text_translations, app_slug, created_at, updated_at)
		values ($1, $2, $3, $4, $5, $6, $7, $8, now(), now())`, coinTableName)
	reasonTextTranslJson, err := json.Marshal(coin.Reason().TextTranslations())
	if err != nil {
		return fmt.Errorf("reason text translations marshal failed: %s", err)
	}
	_, err = r.db.Exec(ctx, sql,
		coin.ID().Value(),
		coin.UserID(),
		coin.Amount(),
		coin.AccruedAt(),
		coin.Reason().ReasonID(),
		coin.Reason().ReasonType().String(),
		reasonTextTranslJson,
		coin.Reason().AppSlug(),
	)
	if err != nil {
		return fmt.Errorf("insert into %s failed: %s", coinTableName, err)
	}
	return nil
}

func (r *CoinDBRepo) GetByUserID(ctx context.Context, userID uint) ([]*domain.Coin, error) {
	sql := fmt.Sprintf(
		`select %s from %s where user_id=$1 order by accrued_at desc`,
		r.defaultSelect(),
		coinTableName,
	)

	rows, err := r.db.Query(ctx, sql, userID)
	if err != nil {
		return nil, fmt.Errorf("failed query %s where user_id=%d: %s", coinTableName, userID, err.Error())
	}
	defer rows.Close()

	coins := make([]*domain.Coin, 0)

	for rows.Next() {
		coin, err := r.rowToCoin(rows)
		if err != nil {
			return nil, fmt.Errorf("failed map row to coin: %v", err)
		}

		coins = append(coins, coin)
	}

	return coins, nil
}

func (r *CoinDBRepo) GetWhereUserIDAndReason(ctx context.Context, userID uint, reason domain.Reason) ([]*domain.Coin, error) {
	sql := fmt.Sprintf(`select %s from %s
		where user_id=$1 and reason_id=$2 and reason_type=$3`, r.defaultSelect(), coinTableName)
	rows, err := r.db.Query(ctx, sql, userID, reason.ReasonID(), reason.ReasonType().String())
	if err != nil {
		return nil, fmt.Errorf("failed query %s where user_id=%d: %s", coinTableName, userID, err.Error())
	}
	defer rows.Close()
	coins := make([]*domain.Coin, 0)
	for rows.Next() {
		coin, err := r.rowToCoin(rows)
		if err != nil {
			return nil, fmt.Errorf("failed map row to coin: %v", err)
		}
		coins = append(coins, coin)
	}
	return coins, nil
}

func (r *CoinDBRepo) GetWhereUserIDAndAmountPositive(ctx context.Context, userID uint) ([]*domain.Coin, error) {
	sql := fmt.Sprintf(`select %s from %s where user_id=$1 and amount > 0`, r.defaultSelect(), coinTableName)
	rows, err := r.db.Query(ctx, sql, userID)
	if err != nil {
		return nil, fmt.Errorf("query failed: %s", err.Error())
	}
	defer rows.Close()
	coins := make([]*domain.Coin, 0)
	for rows.Next() {
		coin, err := r.rowToCoin(rows)
		if err != nil {
			return nil, fmt.Errorf("failed map row to coin: %v", err)
		}
		coins = append(coins, coin)
	}
	return coins, nil
}

func (r *CoinDBRepo) GetWhereUserIDAndAmountNegative(ctx context.Context, userID uint) ([]*domain.Coin, error) {
	sql := fmt.Sprintf(`select %s from %s where user_id=$1 and amount < 0`, r.defaultSelect(), coinTableName)
	rows, err := r.db.Query(ctx, sql, userID)
	if err != nil {
		return nil, fmt.Errorf("query failed: %s", err.Error())
	}
	defer rows.Close()
	coins := make([]*domain.Coin, 0)
	for rows.Next() {
		coin, err := r.rowToCoin(rows)
		if err != nil {
			return nil, fmt.Errorf("failed map row to coin: %v", err)
		}
		coins = append(coins, coin)
	}
	return coins, nil
}

func (r *CoinDBRepo) FirstWhereUserIDReasonTypeDateAppSlug(
	ctx context.Context,
	userID uint,
	reasonType string,
	date time.Time,
	appSlug string,
) (*domain.Coin, error) {
	sql := fmt.Sprintf(`
		select %s from %s
		where user_id=$1
		  and reason_type=$2
		  and app_slug=$3
		  and (accrued_at at time zone '%s')::date=$4
	`, r.defaultSelect(), coinTableName, date.Format("Z07:00"))
	rows := r.db.QueryRow(ctx, sql, userID, reasonType, appSlug, date.Format("2006-01-02"))

	coin, err := r.rowToCoin(rows)
	if err != nil {
		return nil, err

	}

	return coin, nil
}

func (r CoinDBRepo) MakeFromTx(tx app.Tx) domain.Repository {
	r.db = tx.Conn().(pgx.Tx)
	return &r
}

func (r *CoinDBRepo) defaultSelect() string {
	return "id, user_id, amount, accrued_at, reason_id, reason_type, reason_text_translations, app_slug"
}

func (r *CoinDBRepo) rowToCoin(row pgx.Row) (*domain.Coin, error) {
	var (
		uuid                 string
		userID               uint
		amount               int
		accruedAt            time.Time
		reasonID             *string
		reasonType           string
		reasonTextTranslJson []byte
		appSlug              string
	)
	err := row.Scan(&uuid, &userID, &amount, &accruedAt, &reasonID, &reasonType, &reasonTextTranslJson, &appSlug)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, domain.NotFound
		}
		return nil, fmt.Errorf("failed scan row: %s", err)
	}

	reasonTypeVo, err := domain.NewTypeFromString(reasonType)
	if err != nil {
		return nil, fmt.Errorf("failed create reasson type from string %s: %s", reasonType, err)
	}
	var reasonTextTransl map[sharedDomain.Lang]string
	if err := json.Unmarshal(reasonTextTranslJson, &reasonTextTransl); err != nil {
		return nil, fmt.Errorf("failed unmarshal reason text translations from %s: %s", reasonTextTranslJson, err)
	}
	reason := domain.NewReason(
		reasonID,
		*reasonTypeVo,
		reasonTextTransl,
		appSlug,
	)

	return domain.NewCoin(
		*domain.NewCoinIDFromString(uuid),
		userID,
		amount,
		accruedAt,
		*reason,
	), nil
}
