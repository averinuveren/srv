package dto

type CreateDTO struct {
	Name     string
	Email    string
	Password string
}

func NewCreateDTO(name string, email string, password string) *CreateDTO {
	return &CreateDTO{Name: name, Email: email, Password: password}
}
