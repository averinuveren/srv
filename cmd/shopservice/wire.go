//+build wireinject

package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/google/wire"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4/pgxpool"
	cmsApp "shop-service/internal/cmsuser/application"
	cmsRepo "shop-service/internal/cmsuser/infra/repo"
	cuControllers "shop-service/internal/cmsuser/ui/http/controllers"
	coinApp "shop-service/internal/coin/app"
	coinRepo "shop-service/internal/coin/infra/repo"
	coreApp "shop-service/internal/core/app"
	coreRepo "shop-service/internal/core/infra/repo"
	coreControllers "shop-service/internal/core/ui/http/controllers"
	drawApp "shop-service/internal/draw/app"
	drawRepo "shop-service/internal/draw/infra/repo"
	"shop-service/internal/shared/infra/transactionmanager"
	"shop-service/internal/shared/ui/http"
	"shop-service/internal/shared/ui/http/controllers"
	"shop-service/internal/shared/ui/rpc"
	pb "shop-service/internal/shared/ui/rpc/protocol"
)

var cmsAppSet = wire.NewSet(
	cmsRepo.ProvideCmsUserDBRepo,
	cmsApp.ProvideCmsUserService,
)

var drawAppSet = wire.NewSet(
	drawRepo.ProvideDrawDBRepo,
	drawRepo.ProvideTicketDBRepo,
	drawApp.ProvideApplicationImpl,
)

var coinAppSet = wire.NewSet(
	coinRepo.ProvideCoinDBRepo,
	coinApp.ProvideApplicationImpl,
)

var coreAppSet = wire.NewSet(
	drawAppSet,
	coinAppSet,
	coreRepo.ProvideLotDBRepository,
	transactionmanager.ProvideTxManagerDB,
	coreApp.ProvideApplicationImpl,
)

func InitializeRpcService(_ *pgxpool.Pool) pb.ShopServiceServer {
	wire.Build(
		coreAppSet,
		validator.New,
		rpc.ProvideService,
	)
	return &rpc.Service{}
}

func InitializeRoute(_ *pgxpool.Pool) *mux.Router {
	wire.Build(
		http.NewRouter,
		validator.New,
		cmsAppSet,
		coreAppSet,
		controllers.ProvideApiController,
		controllers.ProvideOpenAPIController,
		cuControllers.ProvideCmsUserController,
		coreControllers.ProvideLotController,
	)
	return &mux.Router{}
}
